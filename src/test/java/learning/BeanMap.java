package learning;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.TransformacaoDoArquivo;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

public class BeanMap {

	public static void main(String[] args) throws IOException {
		ColumnPositionMappingStrategy<Acao> strat = new ColumnPositionMappingStrategy<Acao>();
		strat.setType(Acao.class);
		
		String[] columns = new String[] {"componente","horario","ip", "nome", "acao", "informacao"}; // the fields to bind do in your JavaBean
		strat.setColumnMapping(columns);

		CsvToBean<Acao> csv = new CsvToBean<Acao>();
		
		
		final File ARQUIVO = new File("src/test/resources/relatorio.txt");
		
		TransformacaoDoArquivo transformacao = new TransformacaoDoArquivo();
		
		Reader in = transformacao.transforma(ARQUIVO);

		
		List<Acao> list = csv.parse(strat, new CSVReader(in, '\t'));
		
		System.out.println(list.get(0));
		System.out.println(list.get(1));
		System.out.println(list.get(2));
	}
	
	
}
