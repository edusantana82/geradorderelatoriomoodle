package learning;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVReader;

public class LerRelatorio {

	static final File ARQUIVO = new File("src/test/resources/relatorio.txt");
	
	public static void main(String[] args) throws IOException {
	    CSVReader reader = new CSVReader(new FileReader(ARQUIVO), '\t');
	    String [] nextLine;
	    
	    
	    nextLine = reader.readNext();
	    System.out.println("Colunas:");
	    nextLine = reader.readNext();
	    for (String string : nextLine) {
	    	System.out.println(string);
			
		}
	    
	         
	}
	
}
