package spec.main;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.ufpb.moodle.console.Console;

public class HelpTest {

	@Before
	public void setup() {
		
	}
	
	@Test
	public void test_help() throws  IOException{
		String[] args = new String[]{
				"--help"
		};
		Console.main(args);
	}
	@Test
	public void test_needingHelp() throws  IOException{
		String[] args = new String[]{
		};
		Console.main(args);
	}
	
}
