package spec.main;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import edu.ufpb.moodle.console.Console;
import edu.ufpb.moodle.console.MacroCriationOperacao;

public class GenerateMacroScriptTest {

	File INPUT_FILE = new File("src/test/resources/spec/console/planilha-exporta-links.txt"); 
	File OUTUT_FILE = new File("target/macro-ead.imm"); 
	File DEFAULT_FILE = new File(INPUT_FILE.getParentFile(), MacroCriationOperacao.NOME_PADRAO_DO_MACRO); 
	
	@Test
	public void test_gen() throws  IOException {
		
		deleteFileIfExits(OUTUT_FILE);
		
		String[] args = new String[]{
				"-M",
				"-input",
				INPUT_FILE.getAbsolutePath(),
				"-output",
				OUTUT_FILE.getAbsolutePath()
		};
		
		Console.main(args);
		
		Assert.assertTrue(OUTUT_FILE.exists());
		
	}
	
	@Test
	public void test_gen_withou_output() throws  IOException {
		
		deleteFileIfExits(OUTUT_FILE);
		deleteFileIfExits(DEFAULT_FILE);
		
		String[] args = new String[]{
				"-M",
				"-input",
				INPUT_FILE.getAbsolutePath(),
		};
		
		Console.main(args);
		
		Assert.assertTrue(DEFAULT_FILE.exists());
		
	}

	private void deleteFileIfExits(File file) throws  IOException{
		if(file.exists()){
			file.delete();
		}
	}
	
}
