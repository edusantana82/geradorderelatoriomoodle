package spec.main;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import edu.ufpb.moodle.console.Console;

public class GenerateHtmlReportTest {

	private static final FilenameFilter HTML_FILTER = new FilenameFilter() {

		public boolean accept(File arg0, String name) {
			return name.endsWith("html");
		}
	};

	File CONFIG_FILE = new File(
			"src/test/resources/spec/console/mediadores-config.txt");

	File INPUT_DIR = new File(System.getProperty("user.home")
			+ "/iMacros/Downloads");
	File OUTPUT_DIR = new File("target/relatorios");

	// File OUTPUT_DIR= new
	// File("/home/eduardo/Documentos/pedagogia/relatorios/mes-8");

	@Test
	public void testGenerateReports() throws IOException {

		String[] args = new String[] { "-relatorio", "-config",
				CONFIG_FILE.getAbsolutePath(), "-input",
				INPUT_DIR.getAbsolutePath(), "-output",
				OUTPUT_DIR.getAbsolutePath() , "-q"};

		apagaArquivosHtml(OUTPUT_DIR);
		Console.main(args);

		assertRelatoriosCriados();

	}

	@Test()
	public void testGenerateReportsWithDefaultConfig() throws IOException {

		String[] args = new String[] { "-relatorio", 
				"-input",
				INPUT_DIR.getAbsolutePath(), "-output",
				OUTPUT_DIR.getAbsolutePath() , "-q"};

		apagaArquivosHtml(OUTPUT_DIR);
		Console.main(args);

		assertRelatoriosCriados();

	}
	
	@Test()
	public void testGenerateReportsWithDefaultConfigAndMeses() throws IOException {
		
		String[] args = new String[] { "-relatorio", 
				"-m", "9,10",
				"-input",
				INPUT_DIR.getAbsolutePath(), "-output",
				OUTPUT_DIR.getAbsolutePath() , "-q"};
		
		apagaArquivosHtml(OUTPUT_DIR);
		Console.main(args);
		
		assertRelatoriosCriados();
		
	}

	
	@Test()
	public void testSingleReportMeses() throws IOException {

		File INPUT_FILE = new File("src/test/resources/spec/console/604-4639.txt");
		File TARGET_FILE = new File("src/test/resources/spec/console/604-4639.txt.html");
		
		String[] args = new String[] { "-relatorio", 
				"-input",
				INPUT_FILE.getAbsolutePath() , 
				"-m", "9,10", "-q"};

		apagaArquivo(TARGET_FILE);
		Console.main(args);

		Assert.assertTrue(TARGET_FILE.exists());

	}
	
	@Test()
	public void testReportTodosOsMeses() throws IOException {
		
		File INPUT_FILE = new File("src/test/resources/spec/console/604-4639.txt");
		File TARGET_FILE = new File("src/test/resources/spec/console/604-4639.txt.html");
		
		String[] args = new String[] { "-relatorio", 
				"-input",
				INPUT_FILE.getAbsolutePath(), 
				"-intervaloMinimo", "1",
				"-q"};
		
		apagaArquivo(TARGET_FILE);
		Console.main(args);
		
		Assert.assertTrue(TARGET_FILE.exists());
		
	}
	@Test()
	
	public void testIntervaloMinimo() throws IOException {
		
		File INPUT_FILE = new File("src/test/resources/spec/console/604-4639.txt");
		File TARGET_FILE = new File("src/test/resources/spec/console/604-4639.txt.html");
		
		String[] args = new String[] { "-relatorio", 
				"-input",
				INPUT_FILE.getAbsolutePath(), "-q"};
		
		apagaArquivo(TARGET_FILE);
		Console.main(args);
		
		Assert.assertTrue(TARGET_FILE.exists());
		
	}
	
	private void apagaArquivo(File file) {
		if(file.exists()) {
			file.delete();
		}
	}
	
	
	private void assertRelatoriosCriados() {
		File[] files = OUTPUT_DIR.listFiles(HTML_FILTER);
		Assert.assertTrue(files.length > 0);

	}

	private void apagaArquivosHtml(File dir) {
		if (dir.exists()) {
			File[] files = dir.listFiles(HTML_FILTER);
			for (File file : files) {
				file.delete();
			}
		} else {
			dir.mkdirs();
		}
	}

}
