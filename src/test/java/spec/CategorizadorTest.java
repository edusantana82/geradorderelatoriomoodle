package spec;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.MutableDateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.VerificadorDeIP;
import edu.ufpb.moodle.relatorio.categoria.CategorizadorSemanal;
import edu.ufpb.moodle.relatorio.categoria.Conceito;
import edu.ufpb.moodle.relatorio.categoria.ConceitoComDuracao;
import edu.ufpb.moodle.relatorio.categoria.CriterioSemanal;
import edu.ufpb.moodle.relatorio.categoria.RelatorioCategorizado;

public class CategorizadorTest {

	final static int SEMANA_INICIAL = 14;
	final static String UNIVERSIDADE = "universidade";
	final static String FORA_DA_UNIVERSIDADE = "fora";
	VerificadorDeIP verificador = new VerificadorDeIP(UNIVERSIDADE);


	@Test
	public void creating() {
		List<Atividade> list = createAtividades(SEMANA_INICIAL, UNIVERSIDADE,
				2, 3);
		Assert.assertEquals(list.size(), 2);
		Atividade atividade0 = list.get(0);
		Duration duration0 = new Duration(atividade0.getInterval()
				.getStartMillis(), atividade0.getInterval().getEndMillis());
		Assert.assertEquals(duration0.getMillis() / (60 * 60 * 1000), 2);
	}

	private List<Atividade> createAtividades(int semana, String ip,
			int... horas) {

		List<Atividade> result = new LinkedList<Atividade>();
		for (int horasTrabalhas : horas) {
			MutableDateTime time = new MutableDateTime();
			time.setWeekOfWeekyear(semana);
			time.setHourOfDay(0);
			time.setMinuteOfHour(0);

			Interval interval = new Interval(time,
					new DateTime(time).plus(horasTrabalhas * 1000 * 60 * 60));
			Atividade atividade = new Atividade(interval, null, ip);
			result.add(atividade);
		}
		return result;
	}

	
	@SuppressWarnings("unchecked")
	@DataProvider(name = "boas")
	public Object[][] semanasBoas() {
		Object[][] result = new Object[][] { 
				//
				{ createRelatorio(createAtividades(SEMANA_INICIAL, UNIVERSIDADE, 6)),
				new Conceito[] {Conceito.REGULAR} 
				},
				//
				{ createRelatorio(createAtividades(SEMANA_INICIAL, UNIVERSIDADE, 6), createAtividades(SEMANA_INICIAL, FORA_DA_UNIVERSIDADE, 6)),
					new Conceito[] {Conceito.REGULAR} 
				},
				//
				{ createRelatorio(createAtividades(SEMANA_INICIAL, UNIVERSIDADE, 4)),
					new Conceito[] {Conceito.IRREGULAR} 
				},
				//
				{ createRelatorio(createAtividades(SEMANA_INICIAL, UNIVERSIDADE, 4), createAtividades(SEMANA_INICIAL+1, UNIVERSIDADE, 7)),
					new Conceito[] {Conceito.IRREGULAR, Conceito.REGULAR} 
				},
				//
				{ createRelatorio(createAtividades(SEMANA_INICIAL, UNIVERSIDADE, 4), createAtividades(SEMANA_INICIAL+1, UNIVERSIDADE, 7), createAtividades(SEMANA_INICIAL+1, UNIVERSIDADE, 4)),
					new Conceito[] {Conceito.IRREGULAR, Conceito.BOM} 
				},
				//
				{ createRelatorio(createAtividades(SEMANA_INICIAL, UNIVERSIDADE, 4), createAtividades(SEMANA_INICIAL+1, UNIVERSIDADE, 7), createAtividades(SEMANA_INICIAL+1, FORA_DA_UNIVERSIDADE, 4)),
					new Conceito[] {Conceito.IRREGULAR, Conceito.REGULAR} 
				},
				//

		};
		return result;
	}

	
	@Test(dataProvider = "boas")
	public void testCategorizadorSemanal(RelatorioDeUsuario relatorio,
			Conceito[] conceitos) {
		CriterioSemanal criterio = new CriterioSemanal(verificador, 6, 10);
		CategorizadorSemanal categorizador = new CategorizadorSemanal(criterio);

		RelatorioCategorizado result = categorizador.categorizaSemanas(
				relatorio, SEMANA_INICIAL, SEMANA_INICIAL+conceitos.length-1);
		for (int i = 0; i < conceitos.length; i++) {
			int semana = SEMANA_INICIAL + i;
			Conceito conceito = result.getConceitoDaSemana(semana);
			
			Assert.assertEquals(conceito, conceitos[i], "Semana: " + (semana) + " duracao:" + result.getDuracaoDaSemana(semana));
		}

	}

	private RelatorioDeUsuario createRelatorio(List<Atividade>... lists) {
		List<Atividade> atividades = new LinkedList<Atividade>();
		for (List<Atividade> list : lists) {
			atividades.addAll(list);
		}
		return new RelatorioDeUsuario(null, "componente", atividades, null);
	}

	@SuppressWarnings("unchecked")
	@DataProvider(name = "criterioSemanal")
	public Object[][] criterioSemanalBom() {
		Object[][] result = new Object[][] {
				//
				{
						createRelatorio(createAtividades(SEMANA_INICIAL,
								UNIVERSIDADE, 4, 4)), SEMANA_INICIAL,
						Conceito.REGULAR },
				//
				{
						createRelatorio(createAtividades(SEMANA_INICIAL,
								UNIVERSIDADE, 4, 4, 4)), SEMANA_INICIAL,
						Conceito.BOM },
				//
				{
						createRelatorio(createAtividades(SEMANA_INICIAL,
								UNIVERSIDADE, 2, 2, 2)), SEMANA_INICIAL,
						Conceito.REGULAR },
				//
				{
						createRelatorio(createAtividades(SEMANA_INICIAL,
								UNIVERSIDADE, 2, 4, 4)), SEMANA_INICIAL,
						Conceito.BOM },
				//
				{
						createRelatorio(createAtividades(SEMANA_INICIAL,
								UNIVERSIDADE, 2, 2, 1)), SEMANA_INICIAL,
						Conceito.IRREGULAR },
				{
						createRelatorio(createAtividades(SEMANA_INICIAL,
								UNIVERSIDADE, 2, 4, 4)), SEMANA_INICIAL+1,
						Conceito.IRREGULAR },
		//

		};
		return result;

	}

	@Test(dataProvider = "criterioSemanal")
	public void criterioSemanal(RelatorioDeUsuario relatorioDeUsuario,
			int semana, Conceito expectedConceito) {
		double LIMITE_RUIM_REGULAR = 6.0;
		double LIMITE_REGULAR_BOM = 10.0;
		CriterioSemanal criterioSemanal = new CriterioSemanal(verificador,
				LIMITE_RUIM_REGULAR, LIMITE_REGULAR_BOM);

		ConceitoComDuracao conceito = criterioSemanal.avalia(semana, relatorioDeUsuario);
		Assert.assertEquals(conceito.getConceito(), expectedConceito);
	}

	@SuppressWarnings("unchecked")
	@DataProvider(name="atividadesForaDaUniversidade")
	public Object[][] atividadesForaDaUniversidade_data(){
		Object[][] result = new Object[][]{
				{createRelatorio(createAtividades(SEMANA_INICIAL, FORA_DA_UNIVERSIDADE, 8,2)), SEMANA_INICIAL, Conceito.IRREGULAR},
		};
		return result ;
	}
	

	@Test(dataProvider = "atividadesForaDaUniversidade")
	public void atividadesForaDaUniversidade(RelatorioDeUsuario relatorioDeUsuario,
			int semana, Conceito expectedConceito) {
		double LIMITE_RUIM_REGULAR = 6.0;
		double LIMITE_REGULAR_BOM = 10.0;
		CriterioSemanal criterioSemanal = new CriterioSemanal(verificador,
				LIMITE_RUIM_REGULAR, LIMITE_REGULAR_BOM);
		
		ConceitoComDuracao conceito = criterioSemanal.avalia(semana, relatorioDeUsuario);
		Assert.assertEquals(conceito.getConceito(), expectedConceito);
	}

}
