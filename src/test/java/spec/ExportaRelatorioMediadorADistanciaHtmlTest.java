package spec;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.GeradorDeRelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.MoodleFileReader;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.exportador.ExportadorDeRelatorioManager;
import edu.ufpb.moodle.relatorio.exportador.MediadorDistanciaHtmlPrivadoExportador;
import edu.ufpb.moodle.relatorio.exportador.MediadorDistanciaPrivadoExportador;

public class ExportaRelatorioMediadorADistanciaHtmlTest {
	
	@Test
	public void exporta() throws IOException{
		final File ARQUIVO = new File("src/test/resources/RelatorioMainTest/10.txt");
		Configuracao configuracao = Configuracao.readConfiguracaoPadrao();
		configuracao.setMeses(4,5,6,7);
		configuracao.setIntervaloMinimo(5);
		configuracao.setLimiteDeIntervalo(30);
		configuracao.addExportador("MediadorDistanciaPrivado");
		configuracao.setDiretorioDeDestino(new File("target"));
		configuracao.setSufixo("_4567");
		
		MoodleFileReader moodleFileReader = new MoodleFileReader();
		List<Acao> acoes = 	moodleFileReader.lerArquivo(ARQUIVO);
		for (Acao acao : acoes) {
			Assert.assertNotNull(acao.getAcao(), acao.toString());
			Assert.assertNotNull(acao.getIp(), acao.toString());
		}
		
		GeradorDeRelatorioDeUsuario gerador = new GeradorDeRelatorioDeUsuario(configuracao);
		RelatorioDeUsuario relatorio = gerador.geraRelatorio(acoes);
		

		MediadorDistanciaHtmlPrivadoExportador exportador = new MediadorDistanciaHtmlPrivadoExportador();
		
		File arquivoDoRelatorio = exportador.getArquivoParaORelatorio(relatorio);
		deleteFileIfExists(arquivoDoRelatorio);
		
		
		exportador.exporta(relatorio);
		Assert.assertTrue(arquivoDoRelatorio.exists());
		
		
	}

	private void deleteFileIfExists(File arquivoDoRelatorio) {
		if(arquivoDoRelatorio.exists()){
			arquivoDoRelatorio.delete();
		}
		Assert.assertFalse(arquivoDoRelatorio.exists());
		
	}

}
