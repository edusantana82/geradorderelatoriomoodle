package spec;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Assert;
import org.junit.Test;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.GeradorDeRelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.MoodleFileReader;
import edu.ufpb.moodle.relatorio.Relatorio;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.Usuario;

public class GeradorDeRelatorioTest {

	
	@Test
	public void geraRelatorio() throws IOException{
		final File ARQUIVO = new File("src/test/resources/relatorio.txt");
		Configuracao configuracao = Configuracao.readConfiguracaoPadrao();
		configuracao.getMeses().add(7);
		configuracao.setIntervaloMinimo(5);
		configuracao.setLimiteDeIntervalo(30);
		
		MoodleFileReader moodleFileReader = new MoodleFileReader();
		List<Acao> acoes = 	moodleFileReader.lerArquivo(ARQUIVO);
		
		GeradorDeRelatorioDeUsuario gerador = new GeradorDeRelatorioDeUsuario(configuracao);
		RelatorioDeUsuario relatorio = gerador.geraRelatorio(acoes);
		
		Usuario usuario = relatorio.getUsuario();
		Assert.assertEquals("KARLA LUCENA DE SOUZA [TDPED***]", usuario.getNome());
		Assert.assertEquals("TCC_PED_111", relatorio.getComponente());
		
		List<Atividade> atividades = relatorio.getAtividades();
		Atividade atividade = atividades.get(1);
		
		DateTime start = new DateTime(2011, 7, 3, 14, 46, 0, 0);
		DateTime end = new DateTime(2011, 7, 3, 15, 2, 0, 0);
		String ip = "187.64.89.67";
		Map<String, Integer> acaoCount = new HashMap<String, Integer>();
		acaoCount.put("course view", 3);
		acaoCount.put("user view all", 1);
		acaoCount.put("user view", 2);
		acaoCount.put("assignment view submission", 6);
		acaoCount.put("assignment view", 1);
		
		
		Atividade expected = new Atividade(new Interval(start, end), acaoCount, ip);
		
		Assert.assertEquals(expected, atividade);
		
	}
	
}
