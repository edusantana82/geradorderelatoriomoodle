package spec.imacro;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import edu.ufpb.moodle.imacro.LeitorDePlanilha;
import edu.ufpb.moodle.imacro.LeitorDePlanilhaHyperlinks;
import edu.ufpb.moodle.imacro.PlanilhaDocentes;
import edu.ufpb.moodle.relatorio.Usuario;
import edu.ufpb.moodle.relatorio.imacro.IMacroScriptGenerator;

public class GerarScriptDoIMacroParaBaixarLogsDoMoodleTest {
	File PLANILHA_DOCENTES = new File("src/test/resources/spec/imacro/GerarScriptDoIMacroParaBaixarLogsDoMoodleTest/lista-hyperlinks.txt");
	public static File SCRIPT = new File("src/test/resources/spec/imacro/GerarScriptDoIMacroParaBaixarLogsDoMoodleTest/script.iim");
	
	
	@Test
	public void geraScriptIMacro()throws IOException {
		
		LeitorDePlanilha leitor = new LeitorDePlanilhaHyperlinks(PLANILHA_DOCENTES);
		
		PlanilhaDocentes docentes = leitor.lerPlanilha();

		IMacroScriptGenerator generator = new IMacroScriptGenerator();
		
		String script = generator.generate(docentes);
		File imacroFile = new File("target/imacro.imm");
		
		generator.createFile(script, imacroFile);
		
		Assert.assertTrue(imacroFile.exists());
		 
	}
	
	@Test
	public void lerPlanilha() throws IOException{
		
		LeitorDePlanilha leitor = new LeitorDePlanilhaHyperlinks(PLANILHA_DOCENTES);
		
		PlanilhaDocentes docentes = leitor.lerPlanilha();
		
		List<Usuario> usuarios = docentes.getUsuarios();
		Usuario usuario = usuarios.get(0);
		
		Assert.assertEquals("393", usuario.getMoodleId());
		Assert.assertEquals("545", usuario.getCourseId());
	}
	
	
	@Test
	public void generateScript() throws IOException{
		String EXPECTED_SCRIPT = getFile(SCRIPT);

		
		PlanilhaDocentes docentes = new PlanilhaDocentes();
		Usuario usuario = new Usuario();
		usuario.setCourseId("494");
		usuario.setMoodleId("651");
		docentes.add(usuario);
		
		
		IMacroScriptGenerator generator = new IMacroScriptGenerator();
		String script = generator.generate(docentes);
		
		Assert.assertEquals(EXPECTED_SCRIPT, script);
	}

	private String getFile(File file) throws IOException {
		return FileUtils.readFileToString(SCRIPT);
	}

}
