package spec;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.EmissorRelatorio;
import edu.ufpb.moodle.relatorio.Exportador;
import edu.ufpb.moodle.relatorio.Relatorio;

public class EmitirRelatorioMensalTest {

	
	@Test
	public void relatorioMensal() throws IOException{
		Configuracao configuracao = new Configuracao();
		configuracao.getMeses().add(6);
		
		EmissorRelatorio emissor = new EmissorRelatorio(configuracao);
		File moodleFile = new File("src/test/resources/relatorio.txt");
		Relatorio relatorio = emissor.geraRelatorio(moodleFile);
		
		File targetFile = configuracao.getFileToExport(relatorio);
		
		verificaQueArquivoNaoExiste(targetFile);
		
		Exportador exportador = new Exportador(configuracao);
		exportador.exporta(relatorio);
		
		verificaArquivoExiste(targetFile);
	}

	private void verificaArquivoExiste(File targetFile) {
		Assert.assertTrue(targetFile.exists());
		
	}

	private void verificaQueArquivoNaoExiste(File targetFile) {
		if(targetFile.exists()){
			targetFile.delete();
		}
		Assert.assertFalse(targetFile.exists());
	}
	
	
}
