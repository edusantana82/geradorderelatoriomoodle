package spec;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.GeradorDeRelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.MoodleFileReader;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.exportador.ExportadorDeRelatorioManager;
import edu.ufpb.moodle.relatorio.exportador.MediadorDistanciaPrivadoExportador;

public class ExportaRelatorioMediadorADistanciaTest {
	
	
	@Test
	public void exporta() throws IOException{
		final File ARQUIVO = new File("src/test/resources/relatorio.txt");
		Configuracao configuracao = Configuracao.readConfiguracaoPadrao();
		configuracao.setMeses(4,5,6,7);
		configuracao.setIntervaloMinimo(5);
		configuracao.setLimiteDeIntervalo(30);
		configuracao.addExportador("MediadorDistanciaPrivado");
		configuracao.setDiretorioDeDestino(new File("target"));
		configuracao.setSufixo("_4567");
		
		MoodleFileReader moodleFileReader = new MoodleFileReader();
		List<Acao> acoes = 	moodleFileReader.lerArquivo(ARQUIVO);
		
		GeradorDeRelatorioDeUsuario gerador = new GeradorDeRelatorioDeUsuario(configuracao);
		RelatorioDeUsuario relatorio = gerador.geraRelatorio(acoes);
		

		MediadorDistanciaPrivadoExportador exportador = new MediadorDistanciaPrivadoExportador();
		
		File arquivoDoRelatorio = new File(configuracao.getDiretorioDeDestino(), relatorio.getComponente() + configuracao.getSufixo() + ".csv");
		deleteFileIfExists(arquivoDoRelatorio);
		
		
		exportador.exporta(relatorio);
		Assert.assertTrue(arquivoDoRelatorio.exists());
		
		
	}

	private void deleteFileIfExists(File arquivoDoRelatorio) {
		if(arquivoDoRelatorio.exists()){
			arquivoDoRelatorio.delete();
		}
		Assert.assertFalse(arquivoDoRelatorio.exists());
		
	}

}
