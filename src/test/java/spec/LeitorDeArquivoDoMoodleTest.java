package spec;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.MoodleFileReader;

public class LeitorDeArquivoDoMoodleTest {


	@Test
	public void lerArquivo() throws IOException{
		final File ARQUIVO = new File("src/test/resources/relatorio.txt");
		
		MoodleFileReader moodleFileReader = new MoodleFileReader();
		List<Acao> acoes = 	moodleFileReader.lerArquivo(ARQUIVO);
		
		Acao acao0 = acoes.get(0);
		Acao acao1 = acoes.get(1);
		//TCC_PED_111	5 julho 2011, 11:03	187.64.57.218	KARLA LUCENA DE SOUZA [TDPED***]	forum view forum	Fórum de notícias
		
		Assert.assertEquals("forum view forum", acao0.getAcao());
		Assert.assertEquals("course view", acao1.getAcao());
	}
	
	@Test
	public void lerArquivos() throws IOException{
		final File[] ARQUIVOS = new File[] { new File(
				
				"src/test/resources/RelatorioMainTest/p125-11949.txt"),
				};

		MoodleFileReader moodleFileReader = new MoodleFileReader();
		for (File file : ARQUIVOS) {
			List<Acao> acoes = 	moodleFileReader.lerArquivo(file);
			Assert.assertFalse(acoes.isEmpty());
			for (Acao acao : acoes) {
				Assert.assertNotNull(acao.getHorario());
			}
		}
		
	}
	
	@Test(expected=FileNotFoundException.class)
	public void arquivoNaoExiste() throws IOException{
		final File ARQUIVO = new File("_____Arquivo-nao-existe.txt");
		MoodleFileReader moodleFileReader = new MoodleFileReader();
		moodleFileReader.lerArquivo(ARQUIVO);
	}
	
}
