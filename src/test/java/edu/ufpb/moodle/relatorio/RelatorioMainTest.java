package edu.ufpb.moodle.relatorio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;


public class RelatorioMainTest {
	
	@Ignore
	@Test
	public void criaConfiguracao() throws FileNotFoundException, IOException{
		
		String[] args = new String[]{
		};
		RelatorioMain.main(args);
		
		
	}
	@Test
	public void variosArquivos() throws FileNotFoundException, IOException{
		
		String[] args = new String[]{
				"src/test/resources/RelatorioMainTest/config.txt",
//				"src/test/resources/RelatorioMainTest/10.txt",
//				"src/test/resources/RelatorioMainTest/14.txt",
//				"src/test/resources/RelatorioMainTest/38.txt",
//				"src/test/resources/RelatorioMainTest/5.txt",
//				"src/test/resources/RelatorioMainTest/3.txt",
//				"src/test/resources/RelatorioMainTest/p114-19982.txt",
				"src/test/resources/RelatorioMainTest/p79-19686.txt",
				
//				"/home/edu/iMacros/Downloads/p114-19982.txt",
//				"/home/edu/iMacros/Downloads/p125-11949.txt",
//				"/home/edu/iMacros/Downloads/p133-15100.txt",
//				"/home/edu/iMacros/Downloads/p134-1885.txt",
//				"/home/edu/iMacros/Downloads/p136-1452.txt",
//				"/home/edu/iMacros/Downloads/p43-651.txt",
//				"/home/edu/iMacros/Downloads/p46-11952.txt",
//				"/home/edu/iMacros/Downloads/p46-15107.txt",
//				"/home/edu/iMacros/Downloads/p52-15103.txt",
//				"/home/edu/iMacros/Downloads/p62-14686.txt",
//				"/home/edu/iMacros/Downloads/p70-1452.txt",				
		};
		RelatorioMain.main(args);
	}
	
	@Ignore
	@Test
	public void withArgs() throws FileNotFoundException, IOException{
		BufferedReader reader = new BufferedReader(new FileReader("src/test/resources/RelatorioMainTest/args.txt"));
		
		String[] args = reader.readLine().split(" ");
		RelatorioMain.main(args);
	}
	

}
