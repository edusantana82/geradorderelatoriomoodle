package edu.ufpb.moodle.relatorio;

import static org.junit.Assert.*;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class ExtratorDeAtividadesTest {

	
	DateTime MANHA = new DateTime(2011, 7, 5, 8,0,0,0);
	DateTime VIRANDO_O_DIA= new DateTime(2011, 7, 5, 23,40,0,0);

	String ipa = "a";
	String ipb = "b";
	String c = "c";
	
	String t1 = "atividade1";
	String t2 = "atividade2";
	Configuracao configuracao;

	private ExtratorDeAtividades extrator;
	
	@Before
	public void setup() {
		configuracao = new Configuracao();
		configuracao.setLimiteDeIntervalo(30);
		configuracao.setIntervaloMinimo(5);
		extrator = new ExtratorDeAtividades(configuracao);
	}
	class Referencia {
		private final DateTime time;

		Referencia(DateTime time){
			this.time = time;
		}
		public Acao createAcao(int minutos, String ip, String atividade) {
			Acao acaoBean = new Acao();

			acaoBean.setHora(time.plusMinutes(minutos));
			acaoBean.setIp(ip);
			acaoBean.setAcao(atividade);

			return acaoBean;
		}

	}
	
	
	@Test
	public void nenhum() {
		Referencia referencia = new Referencia(MANHA);
		
		List<Atividade> list = extrator.extrair();
		
		Assert.assertEquals(0, list.size());
	}
	
	@Test
	public void apenasUm() {
		Referencia referencia = new Referencia(MANHA);
		Acao acao8 = referencia.createAcao(0, ipa, t1);
		
		List<Atividade> list = extrator.extrair(acao8);
		
		Assert.assertEquals(0, list.size());
	}
	
	
	@Test
	public void doisDentro() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao8 = referencia.createAcao(0, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipa, t1);
		
		List<Atividade> list = extrator.extrair(acao8, acao830);
		
		Assert.assertEquals(1, list.size());
		Atividade atividade = list.get(0);
		
		Assert.assertEquals(new Interval(acao8.getHora(), acao830.getHora()), atividade.getInterval());
	}

	
	@Test
	public void tresDentroDoIntervalo() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao8 = referencia.createAcao(0, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipa, t1);
		Acao acao840 = referencia.createAcao(40, ipa, t1);
		
		List<Atividade> list = extrator.extrair(acao8, acao830, acao840);
		
		Assert.assertEquals(1, list.size());
		Atividade atividade = list.get(0);
		
		Assert.assertEquals(new Interval(acao8.getHora(), acao840.getHora()), atividade.getInterval());
	}

	@Test
	public void quatroDentroDoIntervalo() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao8 = referencia.createAcao(0, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipa, t1);
		Acao acao840 = referencia.createAcao(40, ipa, t1);
		Acao acao850 = referencia.createAcao(50, ipa, t1);
		
		List<Atividade> list = extrator.extrair(acao8, acao830, acao840, acao850);
		
		Assert.assertEquals(1, list.size());
		Atividade atividade = list.get(0);
		
		Assert.assertEquals(atividade.getInterval(), new Interval(acao8.getHora(), acao850.getHora()));
	}	
	
	@Test
	public void doisDentro1Fora() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao8 = referencia.createAcao(0, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipa, t1);
		Acao acao901 = referencia.createAcao(61, ipa, t1);
		
		List<Atividade> list = extrator.extrair(acao8, acao830, acao901);
		
		Assert.assertEquals(1, list.size());
		Atividade atividade = list.get(0);
		
		Assert.assertEquals(new Interval(acao8.getHora(), acao830.getHora()), atividade.getInterval());
	}
	
	@Test
	public void doisDentro1ForaDevidoIP() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao8 = referencia.createAcao(0, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipa, t1);
		Acao acao840 = referencia.createAcao(40, ipb, t1);
		
		List<Atividade> list = extrator.extrair(acao8, acao830, acao840);
		
		Assert.assertEquals(1, list.size());
		Atividade atividade = list.get(0);
		
		Assert.assertEquals(new Interval(acao8.getHora(), acao830.getHora()), atividade.getInterval());
	}
	
	@Test
	public void umForaDoisDentroDevidoIP() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao8 = referencia.createAcao(0, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipb, t1);
		Acao acao840 = referencia.createAcao(40, ipb, t1);
		
		List<Atividade> list = extrator.extrair(acao8, acao830, acao840);
		
		Assert.assertEquals(1, list.size());
		Atividade atividade = list.get(0);
		
		Assert.assertEquals(new Interval(acao830.getHora(), acao840.getHora()), atividade.getInterval());
	}
	
	@Test
	public void tresDentroNovoIpDentro2Dentro() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao800 = referencia.createAcao(0, ipa, t1);
		Acao acao810 = referencia.createAcao(10, ipa, t1);
		Acao acao820 = referencia.createAcao(20, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipb, t1);
		Acao acao840 = referencia.createAcao(40, ipb, t1);
		
		
		List<Atividade> list = extrator.extrair(acao800, acao810, acao820, acao830, acao840);
		
		Assert.assertEquals(2, list.size());
		Atividade atividade = list.get(0);
		Assert.assertEquals(new Interval(acao800.getHora(), acao820.getHora()), atividade.getInterval());
		
		atividade = list.get(1);
		Assert.assertEquals(new Interval(acao830.getHora(), acao840.getHora()), atividade.getInterval());
	}
	
	@Test
	public void menoresQueIntervaloMinimo() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao800 = referencia.createAcao(0, ipa, t1);
		Acao acao810 = referencia.createAcao(2, ipa, t1);
		Acao acao820 = referencia.createAcao(3, ipa, t1);
		Acao acao830 = referencia.createAcao(4, ipb, t1);
		Acao acao840 = referencia.createAcao(5, ipb, t1);
		
		
		List<Atividade> list = extrator.extrair(acao800, acao810, acao820, acao830, acao840);
		
		Assert.assertEquals(0, list.size());
	}
	
	
	@Test
	public void mesmoIntervalo() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao800 = referencia.createAcao(0, ipa, t1);
		Acao acao800_ = referencia.createAcao(0, ipa, t2);
		Acao acao800__ = referencia.createAcao(0, ipa, t1);
		
		
		List<Atividade> list = extrator.extrair(acao800, acao800_, acao800__);
		
		Assert.assertEquals(0, list.size());
	}
	
	
	@Test
	public void contadoresDeAcoesMinimo() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao800 = referencia.createAcao(0, ipa, t1);
		Acao acao810 = referencia.createAcao(10, ipa, t2);
		
		List<Atividade> list = extrator.extrair(acao800, acao810);
		assertEquals(1, list.size());
		
		Atividade atividade1 = list.get(0);
		
		assertEquals(1,	atividade1.getAcaoCount().get(t1).intValue());
		assertEquals(1,	atividade1.getAcaoCount().get(t2).intValue());
	}
	
	@Test
	public void contadoresDeAcoes() {
		Referencia referencia = new Referencia(MANHA);
		
		Acao acao800 = referencia.createAcao(0, ipa, t1);
		Acao acao810 = referencia.createAcao(10, ipa, t2);
		Acao acao820 = referencia.createAcao(20, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipb, t2);
		Acao acao840 = referencia.createAcao(40, ipb, t1);
		
		
		List<Atividade> list = extrator.extrair(acao800, acao810, acao820, acao830, acao840);
		assertEquals(2, list.size());
		
		Atividade atividade1 = list.get(0);
		Atividade atividade2 = list.get(1);
		
		assertEquals(2,	atividade1.getAcaoCount().get(t1).intValue());
		assertEquals(1,	atividade1.getAcaoCount().get(t2).intValue());
		
		
		assertEquals(1,	atividade2.getAcaoCount().get(t1).intValue());
		assertEquals(1,	atividade2.getAcaoCount().get(t2).intValue());
	}
	
	
	@Test
	public void virandoODia(){
		Referencia referencia = new Referencia(VIRANDO_O_DIA);
		
		Acao acao800 = referencia.createAcao(0, ipa, t1);
		Acao acao810 = referencia.createAcao(10, ipa, t2);
		Acao acao820 = referencia.createAcao(20, ipa, t1);
		Acao acao830 = referencia.createAcao(30, ipa, t2);
		Acao acao840 = referencia.createAcao(40, ipa, t1);
		
		
		List<Atividade> list = extrator.extrair(acao800, acao810, acao820, acao830, acao840);
		assertEquals(1, list.size());
		
		Atividade atividade1 = list.get(0);
		Interval expectedInterval = new Interval(acao800.getHora(), acao840.getHora());
		
		assertEquals(expectedInterval, atividade1.getInterval());
		
		
	}
	


}
