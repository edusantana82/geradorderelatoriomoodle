package edu.ufpb.moodle.relatorio.exportador;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class MediadorDistanciaHtmlPrivadoExportadorTest {

	MediadorDistanciaHtmlPrivadoExportador exportador;
	File tempFile;
	
	@Before
	public void setUp() throws Exception {
		tempFile = File.createTempFile("temp", "file");
		tempFile.deleteOnExit();
		
		exportador = new MediadorDistanciaHtmlPrivadoExportador(){
			@Override
			public File getArquivoParaORelatorio(RelatorioDeUsuario relatorio) {
				return tempFile;
			}
			@Override
			protected void writeHtmlHead(BufferedWriter writer,
					RelatorioDeUsuario relatorio) throws IOException {
				// TODO Auto-generated method stub
				super.writeHtmlHead(writer, relatorio);
			}
		};
	}

	@Test
	public void testExporta() throws IOException {

		StringWriter string = new StringWriter();
		BufferedWriter writer = new BufferedWriter(string);
		exportador.writeHtmlHead(writer, null);
		
		writer.flush();
		assertHeaderContainsCSSFile(string.toString());
		
	}

	private void assertHeaderContainsCSSFile(String stringWriter) {

		String cssFile = getCssFileContains();
		String head = stringWriter.toString();
		assertTrue(head.contains(cssFile));
		
	}

	private String getCssFileContains() {
		StringBuilder builder = new StringBuilder();
		
		try {
			FileReader fileReader = new FileReader("src/main/resources/relatorios.css");
			
			
			int c;
			while ((c = fileReader.read()) != -1){
				builder.append( (char) c);
			}
		} catch (IOException e) {
			fail();
		}
		return builder.toString();
	}

}
