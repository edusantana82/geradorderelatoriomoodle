package edu.ufpb.moodle.relatorio.exportador;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.easymock.EasyMock;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;
import org.joda.time.ReadableInstant;
import org.junit.Before;
import org.junit.Test;

import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class ResumoWriterTest {

	private BufferedWriter writer;
	private RelatorioDeUsuario relatorio;
	private LocalDate dia10_1 = new LocalDate(2011, 10, 1);
	private LocalDate dia10_2 = new LocalDate(2011, 10, 2);
	
	
	

	@Before
	public void setUp() throws Exception {
		createRelatorio();
	}


	private void createRelatorio() {
		relatorio = new RelatorioDeUsuario();
		List<Atividade> atividades = new LinkedList<Atividade>();
		relatorio.setAtividades(atividades);
		addAtividade(time(10,1,8), time(10,1,12));
		addAtividade(time(10,1,14), time(10,1,18));
		
		addAtividade(time(10,2,14), time(10,2,21));
		
		addAtividade(time(10,26,20), time(10,26,21));
	}


	private void addAtividade(MutableDateTime time, MutableDateTime time2) {
		Interval interval = new Interval(time, time2);
		Atividade atividade = new Atividade(interval, null, null);
		relatorio.getAtividades().add(atividade);
		
	}


	private static MutableDateTime time(int monthOfYear, int dayOfMonth, int hourOfDay) {
		MutableDateTime dateTime = new MutableDateTime();
		dateTime.setYear(2011);
		dateTime.setMonthOfYear(monthOfYear);
		dateTime.setDayOfMonth(dayOfMonth);
		dateTime.setHourOfDay(hourOfDay);
		dateTime.setMillisOfSecond(0);
		return dateTime;
	}


	@Test
	public void testCreateResumo() throws IOException{
		ResumoWriter resumoWriter = new ResumoWriter();
		Resumo resumo = resumoWriter.createResumo(relatorio);
		assertResumoCreated(resumo);
	}
	
	
	
	
	@Test
	public void test() throws IOException {
		createRelatorioConfig();
		
		ResumoWriter resumoWriter = new ResumoWriter();
		Resumo resumo = resumoWriter.createResumo(relatorio);
		resumoWriter.writeResumo(writer, resumo);
		
		EasyMock.verify(writer);
	}

	private void assertResumoCreated(Resumo resumo) {
		assertNotNull(resumo);
		
		assertEquals(8L*60*60*1000, resumo.getTimes(dia10_1));
		assertEquals(7L*60*60*1000, resumo.getTimes(dia10_2));
		
		
	}

	private void createRelatorioConfig() throws IOException {
		String EXPECTED_STRING = "Dias: 1/10 2/10 26/10";
		
		
		writer = EasyMock.createMock(BufferedWriter.class);
		writer.write(EXPECTED_STRING);
		
		EasyMock.replay(writer);
		
	}

}
