package edu.ufpb.moodle.relatorio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

public class RelatorioFromArgs {
	
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader("src/test/resources/RelatorioFromArgs/args.txt"));
		
		String[] args2 = reader.readLine().split(" ");
		RelatorioMain.main(args2);
	}
}
