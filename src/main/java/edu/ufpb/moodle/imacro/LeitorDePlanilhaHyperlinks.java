package edu.ufpb.moodle.imacro;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;

import edu.ufpb.moodle.relatorio.Usuario;

public class LeitorDePlanilhaHyperlinks implements LeitorDePlanilha {

	private final File file;

	public LeitorDePlanilhaHyperlinks(File file) {
		this.file = file;
	}

	public PlanilhaDocentes lerPlanilha() throws IOException {
		PlanilhaDocentes docentes = new PlanilhaDocentes();
		
		List<Usuario> usuarios = new LinkedList<Usuario>();
		
		List<String> lines = FileUtils.readLines(file);
		for (String string : lines) {
			try {
				Usuario usuario = createUsuarioFromLine(string);
				usuarios.add(usuario);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		

		Collections.sort(usuarios, new ComparadorPorComponenteId());
		docentes.add(usuarios);
		return docentes;
	}

	private Usuario createUsuarioFromLine(String string) {
		
		// string is like: http://www.ead.ufpb.br/user/view.php?id=651&course=494
		
		StringTokenizer st = new StringTokenizer(string, "=&");
		
		st.nextToken();
		String userId = st.nextToken();
		st.nextToken();
		String courseId = st.nextToken();
		
		Usuario usuario = new Usuario();
		usuario.setMoodleId(userId);
		usuario.setCourseId(courseId);
		
		return usuario;
		
	}

}
