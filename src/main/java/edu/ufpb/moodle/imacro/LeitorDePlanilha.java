package edu.ufpb.moodle.imacro;

import java.io.IOException;

public interface LeitorDePlanilha {

	PlanilhaDocentes lerPlanilha() throws IOException;

}
