package edu.ufpb.moodle.imacro;

import java.util.Comparator;

import edu.ufpb.moodle.relatorio.Usuario;

public class ComparadorPorComponenteId implements Comparator<Usuario> {

	public int compare(Usuario first, Usuario second) {
		
		int result;
		
		int componente = new Integer(first.getCourseId()).compareTo(new Integer(second.getCourseId()));
		if(componente==0){
			result = new Integer(first.getMoodleId()).compareTo(new Integer(second.getMoodleId()));
		}else{
			result = componente;
		}
		
		return result;
	}

}
