package edu.ufpb.moodle.imacro;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import edu.ufpb.moodle.relatorio.Usuario;

public class PlanilhaDocentes {
	List<Usuario> usuarios = new LinkedList<Usuario>();
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void add(Collection<Usuario> usuarios) {
		for (Usuario usuario : usuarios) {
			add(usuario);
		}
		
	}
	
	public void add(Usuario usuario) {
		usuarios.add(usuario);
	}

}
