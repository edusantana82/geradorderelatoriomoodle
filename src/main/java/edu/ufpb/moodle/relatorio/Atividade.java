package edu.ufpb.moodle.relatorio;

import java.util.Map;

import org.joda.time.Interval;

public class Atividade {

	Interval interval;
	private Map<String, Integer> acaoCount;
	private String ip;


	public Atividade(Interval interval, Map<String, Integer> acaoCount,
			String ip) {
				this.interval = interval;
				this.acaoCount = acaoCount;
				this.ip = ip;
	}


	public Map<String, Integer> getAcaoCount() {
		return acaoCount;
	}

	public String getIp() {
		return ip;
	}

	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((acaoCount == null) ? 0 : acaoCount.hashCode());
		result = prime * result
				+ ((interval == null) ? 0 : interval.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atividade other = (Atividade) obj;
		if (acaoCount == null) {
			if (other.acaoCount != null)
				return false;
		} else if (!acaoCount.equals(other.acaoCount))
			return false;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Atividade [interval=" + interval + ", acaoCount=" + acaoCount
				+ ", ip=" + ip + "]";
	}



}
