package edu.ufpb.moodle.relatorio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import au.com.bytecode.opencsv.CSVReader;

public class TransformacaoDoArquivo {

	public TransformacaoDoArquivo() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws IOException {

		final File ARQUIVO = new File("src/test/resources/relatorio.txt");

		TransformacaoDoArquivo transformacao = new TransformacaoDoArquivo();

//		Reader in = transformacao.transforma(ARQUIVO);
		Reader in = transformacao.transforma2(ARQUIVO);

		transformacao.printLines(in, 2);

	}


	private void printLines(Reader in, int lines) throws IOException {

		System.out.println();
		System.out.println();
		CSVReader reader = new CSVReader(in, '\t');
		String[] nextLine;

		for (int i = 0; i < lines; i++) {
			nextLine = reader.readNext();
			System.out.println(nextLine[4]);
		}

	}

	public Reader transforma2(File file) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(file));
		in.readLine(); // ignore first line
		in.readLine();

		return in;
	}

	
	public Reader transforma(File file) throws IOException {

		BufferedReader in = new BufferedReader(new FileReader(file));
		in.readLine(); // ignore first line

		CharArrayWriter charArray = new CharArrayWriter();

		BufferedWriter out = new BufferedWriter(charArray);
		String line = in.readLine();
		String substring = line.substring(52); // ignore second line error.
		out.write(substring);
		out.write("\n");

		char[] buff = new char[1024];
		int read;
		int i=0;
		while ((read = in.read(buff)) > 0) {
			out.write(buff, 0, read);
		}
		

		return new CharArrayReader(charArray.toCharArray());
	}

}
