package edu.ufpb.moodle.relatorio;

import java.util.LinkedList;
import java.util.List;

public class VerificadorDeIP {
	private final List<String> ipsPermitidos;

	public VerificadorDeIP(String...ips) {
		ipsPermitidos = new LinkedList<String>();
		for (String ip: ips) {
			ipsPermitidos.add(ip);
		}
	}
	public VerificadorDeIP(List<String> ipsPermitidos) {
		this.ipsPermitidos = ipsPermitidos;
	}

	public boolean isIPPermitido(String ip) {

		boolean permitido = ipsPermitidos.isEmpty();
		
		for (String ipPermitido: ipsPermitidos) {
			permitido |= ip.startsWith(ipPermitido);
		}
		
		
		return permitido;
	}

}
