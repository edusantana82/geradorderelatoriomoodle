package edu.ufpb.moodle.relatorio;

import java.text.ParseException;

public class RelatorioException extends RuntimeException{

	public RelatorioException(ParseException e) {
		super(e);
	}

}
