package edu.ufpb.moodle.relatorio;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.apache.log4j.Logger;
import org.joda.time.Duration;
import org.joda.time.Interval;

import edu.ufpb.moodle.relatorio.handler.AcaoHandler;
import edu.ufpb.moodle.relatorio.handler.EstadoDaFila;
import edu.ufpb.moodle.relatorio.handler.PontoInicial;

public class ExtratorDeAtividades {
	static final Logger  logger = Logger.getLogger(ExtratorDeAtividades.class);

	private final Configuracao configuracao;

	public ExtratorDeAtividades(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	public List<Atividade> extrair(Acao...acoes) {
		return this.extrair(Arrays.asList(acoes));
		
	}
	public List<Atividade> extrair(List<Acao> acoes) {
		logger.debug(" ----- extrair ------");
		
		LinkedList<Acao> fila = new LinkedList<Acao>(acoes);
		List<Atividade> result = new LinkedList<Atividade>();

		EstadoDaFila estado = new EstadoDaFila();
		AcaoHandler handler = new PontoInicial(configuracao);
		
		while (estado.continua){
			logger.debug("Handler: "+handler.getClass().getSimpleName());
			handler.handle(estado, fila, result);
			handler = handler.nextHandler(estado, fila);
		}
		
		return result;
	}
	


	

	
	
	


	



}
