package edu.ufpb.moodle.relatorio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import edu.ufpb.moodle.relatorio.handler.FinalizaIntervalo;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;


public class MoodleFileReader {

	static final Logger  logger = Logger.getLogger(MoodleFileReader.class);
	
	public List<Acao> lerArquivo(File file) throws IOException {
		seArquivoNaoExisteError(file);
		
		TransformacaoDoArquivo transformacao = new TransformacaoDoArquivo();
		
		Reader in = transformacao.transforma2(file);

		List<Acao> list = readAcoesAsBeans2(in, file);
		list = removeAcoesInvalidas(list, file);
		
		debug(list, file);
		
		return list;
	}




	private void seArquivoNaoExisteError(File file) {
		// TODO Auto-generated method stub
		
	}




	private List<Acao> readAcoesAsBeans2(Reader reader, File file) throws IOException {
		


		BufferedReader in = new BufferedReader(reader);
		
		String line;
		List<Acao> result = new LinkedList<Acao>();
		int index=2;
		while( (line = in.readLine())!=null){
			String[] valores = line.split("\t");
			Acao acao = createAcao(valores);
			if(acao==null || !isAcaoValida(acao)){
				logger.info("Destartando linha("+index+"): "+line);
			}else{
				result.add(acao);
			}
			index++;
			
		}
		
		logger.debug("Acões lidas("+file.getName()+"): " + result.size());
		
		
		return result;
	}
	private Acao createAcao(String[] line) {
		// {"[0]componente","[1]horario","[2]ip", "[3]nome", "[4]acao", "[5]informacao"}; // the fields to bind do in your JavaBean
		Acao acao;
		try {
			acao = new Acao();
			acao.setComponente(line[0].trim());
			acao.setHorario(line[1].trim());
			acao.setIp(line[2].trim());
			acao.setNome(line[3].trim());
			acao.setAcao(line[4].trim());
			acao.setInformacao(line[5].trim());
		} catch (ArrayIndexOutOfBoundsException e) {
			acao = null;
		}
		return acao;
	}




	private List<Acao> readAcoesAsBeans(Reader in, File file) {
		ColumnPositionMappingStrategy<Acao> strat = new ColumnPositionMappingStrategy<Acao>();
		strat.setType(Acao.class);
		
		String[] columns = new String[] {"componente","horario","ip", "nome", "acao", "informacao"}; // the fields to bind do in your JavaBean
		strat.setColumnMapping(columns);

		CsvToBean<Acao> csv = new CsvToBean<Acao>();
		List<Acao> list = csv.parse(strat, new CSVReader(in, '\t'));
		logger.debug("Acões lidas("+file.getName()+"): " + list.size());
		
		
		return list;
	}

	private List<Acao> removeAcoesInvalidas(List<Acao> list, File file) {
		List<Acao> result = new LinkedList<Acao>();
		boolean algumaAcaoRemovida = false;
		for (Acao acao : list) {
			if(isAcaoValida(acao)){
				result.add(acao);
			}else{
				logger.warn("Acao ignorada("+file.getName()+"):"+ acao);
				algumaAcaoRemovida = true;
			}
		}

		if(algumaAcaoRemovida){
			logger.warn("Total de Acao ignoradas("+file.getName()+"):"+ (list.size() - result.size()));
		}
		
		return result;
	}




	
	private boolean isAcaoValida(Acao acao) {
		/**
		 * Arquivo de log possui erro, ver aquivo p115-11949.txt:
		 */  
// colunas:
// Curso	Hora	Endereço IP	Nome completo	Ação	Informação
//		CNEI-II_PED_111	 22 fevereiro 2011, 22:10	187.115.180.134	VIRGINIA FARIAS PEREIRA DE ARAUJO [PRPED***20092]	course view	Ciências Naturais na Educação Infantil II - PED - 2011.1 
//		CNEI-II_PED_111	 22 fevereiro 2011, 22:10	187.115.180.134	VIRGINIA FARIAS PEREIRA DE ARAUJO [PRPED***20092]	label update	 
//		Aula 2: Quem compõe o Meio Ambiente?
//		Olá Apren... 
//		CNEI-II_PED_111	 22 fevereiro 2011, 22:10	187.115.180.134	VIRGINIA FARIAS PEREIRA DE ARAUJO [PRPED***20092]	course update mod	label 376 
//		CNEI-II_PED_111	 22 fevereiro 2011, 22:09	187.115.180.134	VIRGINIA FARIAS PEREIRA DE ARAUJO [PRPED***20092]	resource view	Objeto de Aprendizagem - Aula I.2 

		
		return acao.getHorario() != null && acao.getIp() != null
				&& acao.getNome() != null && acao.getAcao() != null;
	}


	private void debug(List<Acao> list, File file) throws IOException {
		CSVWriter writer = new CSVWriter(new FileWriter("debug.csv"));
		int i = 0;
		for (Acao acao : list) {
			
			writer.writeNext(new String[]{
					""+(++i),
					acao.getHorario(),
					acao.getAcao(),
			});
		}

		writer.close();
	}



}
