package edu.ufpb.moodle.relatorio.imacro;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;

import edu.ufpb.moodle.imacro.ComparadorPorComponenteId;
import edu.ufpb.moodle.imacro.PlanilhaDocentes;
import edu.ufpb.moodle.relatorio.Usuario;

public class IMacroScriptGenerator {

	String INICIO_DO_ARQUIVO = "VERSION BUILD=7210419 RECORDER=FX\nTAB T=1\nURL GOTO=http://www.ead.ufpb.br/course/report.php?id=106\n";
	
	public String generate(PlanilhaDocentes docentes) {
		StringBuilder sb = new StringBuilder(INICIO_DO_ARQUIVO);
		
		LinkedList<Usuario> usuarios = new LinkedList<Usuario>(docentes.getUsuarios());
		Collections.sort(usuarios, new ComparadorPorComponenteId());
		
		int index=0;
		for (Usuario usuario : usuarios) {
			sb.append(getUserLines(usuario, ++index));
		}
		
		return sb.toString();
	}

	private String getUserLines(Usuario usuario, int index) {
		String nomeDoArquivo = getNomeDoArquivo(usuario);
		StringBuilder sb = new StringBuilder("\n");
		sb.append("'"+index+"\n");
		sb.append("WAIT SECONDS=10\n");
		sb.append("ONDOWNLOAD FOLDER=* FILE="+nomeDoArquivo+".txt WAIT=YES\n");
		sb.append("URL GOTO=http://www.ead.ufpb.br/course/report/log/index.php?chooselog=1&showusers=1&showcourses=1&id="+usuario.getCourseId()+"&group=0&user="+usuario.getMoodleId()+"&date=0&modid=&modaction=0&logformat=downloadascsv\n");
		
		return sb.toString();
	}

	private String getNomeDoArquivo(Usuario usuario) {
		return usuario.getCourseId()+"-"+usuario.getMoodleId();
	}

	public void createFile(String script, File imacroFile) throws IOException {
	
		FileWriter writer = new FileWriter(imacroFile);
		writer.write(script);
		
		writer.close();
	}

}
