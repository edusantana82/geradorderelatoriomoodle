package edu.ufpb.moodle.relatorio;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

public class FiltraAtividades {

	public List<Atividade> filtra(List<Atividade> atividades,
			Configuracao configuracao) {
		LinkedList<Atividade> result = new LinkedList<Atividade>();
		
		for (Atividade atividade : atividades) {
			DateTime start = atividade.getInterval().getStart();
			if(configuracao.getMeses().contains(start.getMonthOfYear())){
				result.add(atividade);
			}
		}
		return result;
	}

}
