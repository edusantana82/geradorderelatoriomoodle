package edu.ufpb.moodle.relatorio;

import java.util.LinkedList;
import java.util.List;

public class RelatorioDeUsuario extends Relatorio {

	private Usuario usuario;
	private String componente;
	private List<Atividade> atividades = new LinkedList<Atividade>();
	private Configuracao configuracao;

	public RelatorioDeUsuario() {
		super();
	}

	public RelatorioDeUsuario(Usuario usuario, String componente,
			List<Atividade> atividades, Configuracao configuracao) {
		super();
		this.usuario = usuario;
		this.componente = componente;
		this.atividades = atividades;
		this.setConfiguracao(configuracao);
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setComponente(String componente) {
		this.componente = componente;
	}

	public String getComponente() {
		return componente;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setConfiguracao(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	public Configuracao getConfiguracao() {
		return configuracao;
	}


}
