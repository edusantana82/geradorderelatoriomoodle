package edu.ufpb.moodle.relatorio;

import org.joda.time.DateTime;


public class Acao implements Comparable<Acao> {

	String componente;
	String horario;
	String ip;
	String nome;
	String acao;
	String informacao;
	DateTime hora;
	private int line;

	public String getComponente() {
		return componente;
	}

	public void setComponente(String componente) {
		this.componente = componente;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getInformacao() {
		return informacao;
	}

	public void setInformacao(String informacao) {
		this.informacao = informacao;
	}

	public int compareTo(Acao o) {
		return this.hora.compareTo(o.hora);
	}

	public DateTime getHora() {
		return hora;
	}

	public void setHora(DateTime hora) {
		this.hora = hora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acao == null) ? 0 : acao.hashCode());
		result = prime * result
				+ ((componente == null) ? 0 : componente.hashCode());
		result = prime * result + ((hora == null) ? 0 : hora.hashCode());
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		result = prime * result
				+ ((informacao == null) ? 0 : informacao.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Acao other = (Acao) obj;
		if (acao == null) {
			if (other.acao != null)
				return false;
		} else if (!acao.equals(other.acao))
			return false;
		if (componente == null) {
			if (other.componente != null)
				return false;
		} else if (!componente.equals(other.componente))
			return false;
		if (hora == null) {
			if (other.hora != null)
				return false;
		} else if (!hora.equals(other.hora))
			return false;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		if (informacao == null) {
			if (other.informacao != null)
				return false;
		} else if (!informacao.equals(other.informacao))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Acao [componente=" + componente + ", horario=" + horario
				+ ", ip=" + ip + ", nome=" + nome + ", acao=" + acao
				+ ", informacao=" + informacao + ", hora=" + hora + "]";
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getLine() {
		return line;
	}



}
