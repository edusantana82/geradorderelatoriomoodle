package edu.ufpb.moodle.relatorio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import edu.ufpb.moodle.relatorio.exportador.CSSCreator;
import edu.ufpb.moodle.relatorio.exportador.MediadorDistanciaHtmlPrivadoExportador;

public class RelatorioMain {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		ifNotFilesPrintHelp(args);
		
		if(args.length > 1){
			Configuracao configuracao = Configuracao.readConfiguracaoFromFile(args[0]);
			
			for (int i = 1; i < args.length; i++) {
				File file = new File(args[i]);
				
				try {
					MoodleFileReader moodleFileReader = new MoodleFileReader();
					List<Acao> acoes = moodleFileReader.lerArquivo(file);
					GeradorDeRelatorioDeUsuario gerador = new GeradorDeRelatorioDeUsuario(
							configuracao);
					RelatorioDeUsuario relatorio = gerador.geraRelatorio(acoes);
					MediadorDistanciaHtmlPrivadoExportador exportador = new MediadorDistanciaHtmlPrivadoExportador();
					exportador.exporta(relatorio);
				} catch (Exception e) {
					System.err.println("Erro no arquivo: " + file.getAbsolutePath());
					System.err.println(e.getLocalizedMessage());
					//
					e.printStackTrace();
				}
				
			}
			CSSCreator cssCreator = new CSSCreator();
			cssCreator.create(configuracao);
		}
	}

	private static void ifNotFilesPrintHelp(String[] args) throws FileNotFoundException, IOException {
		Configuracao.criaArquivoDeConfiguracaoSeNaoExistir();
		System.out.println("Usagse:");
		System.out.println("configFile [arquivos]+");		
	}

}
