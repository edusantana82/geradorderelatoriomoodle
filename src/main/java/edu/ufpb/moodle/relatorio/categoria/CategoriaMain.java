package edu.ufpb.moodle.relatorio.categoria;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.GeradorDeRelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.MoodleFileReader;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.VerificadorDeIP;

public class CategoriaMain {

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		if (args.length > 1) {
			Configuracao configuracao = Configuracao
					.readConfiguracaoFromFile(args[0]);

			System.out.println(configuracao);
			
			VerificadorDeIP verificador = new VerificadorDeIP(configuracao.getIpsPermitidos());
			CriterioSemanal criterio = new CriterioSemanal(verificador,
					4, 8);
			System.out.println(criterio);
			
			for (int i = 1; i < args.length; i++) {
				File file = new File(args[i]);

				try {
					MoodleFileReader moodleFileReader = new MoodleFileReader();
					List<Acao> acoes = moodleFileReader.lerArquivo(file);
					GeradorDeRelatorioDeUsuario gerador = new GeradorDeRelatorioDeUsuario(
							configuracao);
					RelatorioDeUsuario relatorio = gerador.geraRelatorio(acoes);

					CategorizadorSemanal categorizador = new CategorizadorSemanal(
							criterio);

					RelatorioCategorizado result = categorizador
							.categorizaSemanas(relatorio,
									configuracao.getSemanaInicial(),
									configuracao.getSemanaFinal());
					
					ConceitosCSVExportador exportador = new ConceitosCSVExportador(configuracao);
					exportador.export(result);
				} catch (Exception e) {
					System.err.println("Erro no arquivo: "
							+ file.getAbsolutePath());
					System.err.println(e.getLocalizedMessage());
					//
					e.printStackTrace();
				}

			}
		}

	}

}
