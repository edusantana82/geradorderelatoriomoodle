package edu.ufpb.moodle.relatorio.categoria;

import java.util.HashMap;
import java.util.Map;

import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class CategorizadorSemanal {

	private final CriterioSemanal criterio;

	public CategorizadorSemanal(CriterioSemanal criterio) {
		this.criterio = criterio;
	}

	public RelatorioCategorizado categorizaSemanas(
			RelatorioDeUsuario relatorio, int semanaInical, int semanaFinal) {
		Map<Integer, ConceitoComDuracao> conceitosPorSemana = new HashMap<Integer, ConceitoComDuracao>();

		
		for (int semana = semanaInical; semana <= semanaFinal; semana++) {
			ConceitoComDuracao conceito = criterio.avalia(semana, relatorio);
			conceitosPorSemana.put(semana, conceito);

		}
		return new RelatorioCategorizado(relatorio, criterio, semanaInical, semanaFinal,
				conceitosPorSemana);
	}

}
