package edu.ufpb.moodle.relatorio.categoria;

import java.text.DecimalFormat;

public class ConceitoComDuracao {

	private final Conceito conceito;
	private final double duracaoNaSemana;

	public ConceitoComDuracao(Conceito conceito, double duracaoNaSemana) {
		this.conceito = conceito;
		this.duracaoNaSemana = duracaoNaSemana;
	}

	public Conceito getConceito() {
		return conceito;
	}

	public double getDuracaoNaSemana() {
		return duracaoNaSemana;
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("00");
		int hora = (int) duracaoNaSemana;
		int minutos = (int) ((duracaoNaSemana * 60) % 60);
		
		return conceito	+ "("+ df.format(hora)+ ":"+df.format(minutos)+")";
	}

}
