package edu.ufpb.moodle.relatorio.categoria;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class RelatorioCategorizado {

	private final RelatorioDeUsuario relatorio;
	private final int semanaInical;
	private final int semanaFinal;
	private final Map<Integer, ConceitoComDuracao> conceitosPorSemana;
	private final CriterioSemanal criterio;

	public RelatorioCategorizado(RelatorioDeUsuario relatorio,
			CriterioSemanal criterio, int semanaInical, int semanaFinal, Map<Integer, ConceitoComDuracao> conceitosPorSemana) {
				this.relatorio = relatorio;
				this.criterio = criterio;
				this.semanaInical = semanaInical;
				this.semanaFinal = semanaFinal;
				this.conceitosPorSemana = conceitosPorSemana;
	}

	public List<Integer> getSemanas() {
		List<Integer> semanas = new LinkedList<Integer>(conceitosPorSemana.keySet());
		Collections.sort(semanas);
		return semanas;
	}

	public Conceito getConceitoDaSemana(int semanaInical) {
		return this.conceitosPorSemana.get(semanaInical).getConceito();
	}

	public double getDuracaoDaSemana(int semana) {
		return this.conceitosPorSemana.get(semanaInical).getDuracaoNaSemana();
	}

	public RelatorioDeUsuario getRelatorio() {
		return relatorio;
	}

	public Map<Integer, ConceitoComDuracao> getConceitosPorSemana() {
		return conceitosPorSemana;
	}


}
