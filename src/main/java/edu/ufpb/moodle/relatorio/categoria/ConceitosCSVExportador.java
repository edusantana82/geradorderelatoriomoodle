package edu.ufpb.moodle.relatorio.categoria;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import au.com.bytecode.opencsv.CSVWriter;
import edu.ufpb.moodle.relatorio.Configuracao;

public class ConceitosCSVExportador {

	private final Configuracao configuracao;

	public ConceitosCSVExportador(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	public void export(RelatorioCategorizado result) throws IOException {
		writeConceitos(result);
		writeDetalhes(result);
		
		
	}

	private void writeDetalhes(RelatorioCategorizado result) throws IOException {
		CSVWriter detalhes= new CSVWriter(new FileWriter(getDetalhesFile(), true));
		
		List<String> linha = new LinkedList<String>();
		
		linha.add(result.getRelatorio().getComponente());
		linha.add(result.getRelatorio().getUsuario().getNome());
		
		Map<Integer, ConceitoComDuracao> conceitosPorSemana = result.getConceitosPorSemana();

		List<Integer> semanas = result.getSemanas();
		for (Integer semana: semanas) {
			linha.add(conceitosPorSemana.get(semana).toString());
		}
		
		
		detalhes.writeNext(linha.toArray(new String[linha.size()]));
		detalhes.close();
	}

	private void writeConceitos(RelatorioCategorizado result) throws IOException {
		CSVWriter conceitos = new CSVWriter(new FileWriter(getConceitosFile(), true));
		
		List<Integer> bom = new LinkedList<Integer>();
		List<Integer> regular = new LinkedList<Integer>();
		List<Integer> ruim = new LinkedList<Integer>();

		for (Integer semana : result.getSemanas()) {
			Conceito conceito = result.getConceitoDaSemana(semana);
			switch (conceito) {
			case BOM:
				bom.add(semana);
				break;
			case REGULAR:
				regular.add(semana);
				break;
			case IRREGULAR:
				ruim.add(semana);
			}
		}
		
		String[] linha = new String[]{
				result.getRelatorio().getComponente(), 
				result.getRelatorio().getUsuario().getNome(),
				Integer.toString(bom.size()),Integer.toString(regular.size()),Integer.toString(ruim.size()),
				};
		conceitos.writeNext(linha);
		conceitos.close();
	}

	private File getDetalhesFile() {
		return new File(configuracao.getDiretorioDeDestino(), "detalhes.csv");
	}

	private File getConceitosFile() {
		File conceitos = new File(configuracao.getDiretorioDeDestino(), "conceitos.csv");
		return conceitos;
	}

}
