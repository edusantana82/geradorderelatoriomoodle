package edu.ufpb.moodle.relatorio.categoria;

import java.util.LinkedList;
import java.util.List;
import javax.naming.LimitExceededException;

import org.joda.time.Duration;
import org.joda.time.Interval;

import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.VerificadorDeIP;

public class CriterioSemanal {


	/**
	 * Em horas
	 */
	private final double limiteRuimRegular;
	/**
	 * Em horas
	 */
	private final double limiteRegularBom;
	private final VerificadorDeIP verificador;

	public CriterioSemanal(VerificadorDeIP verificador, double limiteRuimRegular, double limiteRegularBom) {
		this.verificador = verificador;
		this.limiteRuimRegular = limiteRuimRegular;
		this.limiteRegularBom = limiteRegularBom;
	}

	public ConceitoComDuracao avalia(int semana, RelatorioDeUsuario relatorioDeUsuario) {
		List<Interval> atividadesDaSemana = new LinkedList<Interval>();
		for (Atividade atividade : relatorioDeUsuario.getAtividades()) {
			if(atividade.getInterval().getStart().getWeekOfWeekyear() == semana){
				if(verificador.isIPPermitido(atividade.getIp())){
					atividadesDaSemana.add(atividade.getInterval());
				}
			}
		}
		
		double duracaoNaSemana = 0;
		
		for (Interval interval : atividadesDaSemana) {
			Duration duration = new Duration(interval.getStart(), interval.getEnd());
			duracaoNaSemana += duration.getMillis()/(60*60*1000.0);
			
		}
		
		ConceitoComDuracao conceito;

		if (duracaoNaSemana < limiteRuimRegular) {
			conceito = new ConceitoComDuracao(Conceito.IRREGULAR, duracaoNaSemana);
		} else if (duracaoNaSemana < limiteRegularBom) {
			conceito = new ConceitoComDuracao(Conceito.REGULAR, duracaoNaSemana);
		} else {
			conceito = new ConceitoComDuracao(Conceito.BOM, duracaoNaSemana);
		}
		
		
		return conceito;
	}

	@Override
	public String toString() {
		return "CriterioSemanal [limiteRuimRegular=" + limiteRuimRegular
				+ ", limiteRegularBom=" + limiteRegularBom + "]";
	}

}
