package edu.ufpb.moodle.relatorio.handler;

import java.util.LinkedList;
import java.util.List;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;

public class PontoInicial extends AcaoHandler {

	public PontoInicial(Configuracao configuracao) {
		super(configuracao);
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {

	}

	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado, LinkedList<Acao> fila) {
		AcaoHandler result;

		if (fila.isEmpty()) {
			result = new Quit(configuracao);
		} else {
			result = new IniciaIntervaloDaFila(configuracao);
		}

		return result;

	}

}
