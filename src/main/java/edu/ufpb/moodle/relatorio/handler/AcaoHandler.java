package edu.ufpb.moodle.relatorio.handler;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.Duration;
import org.joda.time.Interval;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;

public abstract class AcaoHandler {
	static final Logger logger = Logger.getLogger(AcaoHandler.class);

	Configuracao configuracao;

	public AcaoHandler(Configuracao configuracao) {
		super();
		this.configuracao = configuracao;
	}

	public abstract void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result);

	public abstract AcaoHandler nextHandler(EstadoDaFila estado,
			LinkedList<Acao> fila);

	boolean isNovaAcaoNoIntervalo(Acao peek, EstadoDaFila estado) {

		boolean result;
		if (isIpsDiferentes(estado, peek)) {
			result = false;
		} else {
			int limiteDeIntervalo = configuracao.getLimiteDeIntervalo();
			Duration dur = new Duration(estado.acoes.peekLast().getHora(),
					peek.getHora());
			result = dur.getMillis() <= limiteDeIntervalo * 60000;
		}
		return result;
	}

	boolean isIpsDiferentes(EstadoDaFila estado, Acao peek) {
		return !peek.getIp().equals(estado.acoes.peekLast().getIp());
	}

	@Deprecated
	void incrementaContadoresDeAtividadeComBaseNasAcoes(EstadoDaFila estado) {
		estado.incrementCountersFromCurrentAcao();

	}

	void adicionaAtividade(EstadoDaFila estado, List<Atividade> result) {
		Interval interval = new Interval(estado.start, estado.end);
		estado.atividade.setInterval(interval);
		result.add(estado.atividade);
		estado.clear();
	}

	void addAcaoFromCurrentAcao(EstadoDaFila estado) {
		estado.acoes.add(estado.currentAcao);
		logger.debug(estado.acoes.size() + ") Acao para ser adicionada: "
				+ estado.currentAcao);

	}

}
