package edu.ufpb.moodle.relatorio.handler;

import java.util.LinkedList;
import java.util.List;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;

public class DescartaAcao extends AcaoHandler {

	public DescartaAcao(Configuracao configuracao) {
		super(configuracao);
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {
		estado.clear();
	}

	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado, LinkedList<Acao> fila) {
		return new PontoInicial(configuracao);
	}

}
