package edu.ufpb.moodle.relatorio.handler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.Duration;
import org.joda.time.Interval;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.ExtratorDeAtividades;

public class FinalizaIntervalo extends AcaoHandler {
	static final Logger  logger = Logger.getLogger(FinalizaIntervalo.class);

	public FinalizaIntervalo(Configuracao configuracao) {
		super(configuracao);
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {
		
		if(isAcoesMenorQueIntervaloMinimo(estado)){
			// não adiciona ao resultado.
			logDescartaIntervalo(estado);
		}else{
			result.add(criaAtividade(estado));
			logIntervaloAdicionado(estado);
		}
		
		estado.clear();

	}


	private void logIntervaloAdicionado(EstadoDaFila estado) {
		logger.debug("Intervalo ADICIONADO: "+   estado.acoes.peekFirst().getHora()+" - "+
				estado.acoes.peekLast().getHora());
		
	}

	private void logDescartaIntervalo(EstadoDaFila estado) {
		long duracao = getDuracao(estado).getMillis()/60000;
		logger.info("Intervalo descartado("+estado.acoes.peekFirst().getNome()+"): "+   estado.acoes.peekFirst().getHora()+" - "+
				estado.acoes.peekLast().getHora() + "[duracao="+duracao+"]");
		
	}

	private boolean isAcoesMenorQueIntervaloMinimo(EstadoDaFila estado) {
		Duration dur = getDuracao(estado);
		return (dur.getMillis() <= configuracao.getIntervaloMinimo() * 60000);
	}

	private Duration getDuracao(EstadoDaFila estado) {
		Duration dur = new Duration(estado.acoes.peekFirst().getHora(), estado.acoes.peekLast().getHora());
		return dur;
	}

	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado, LinkedList<Acao> fila) {
		return new PontoInicial(configuracao);
	}


	private Atividade criaAtividade(EstadoDaFila estado) {
		Interval interval = new Interval(estado.acoes.peekFirst().getHora(), estado.acoes.peekLast().getHora());
		Map<String, Integer> counters = contaAtividades(estado);
		
		Atividade atividade = new Atividade(interval, counters, estado.acoes.peek().getIp());
		return atividade;
	}

	private Map<String, Integer> contaAtividades(EstadoDaFila estado) {
		Map<String, Integer> counters= new HashMap<String, Integer>();
		for (Acao acao : estado.acoes) {
			
			Integer newContValue;
			Integer count = counters.get(acao.getAcao());
			if (count != null) {
				newContValue = count + 1;
			} else {
				newContValue = 1;
			}
			
			counters.put(acao.getAcao(), newContValue);
	
		}
		return counters;
	}
	
}
