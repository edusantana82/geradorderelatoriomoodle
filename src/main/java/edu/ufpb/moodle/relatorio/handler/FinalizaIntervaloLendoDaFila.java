package edu.ufpb.moodle.relatorio.handler;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.Interval;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;

/**
 * Atualiza from fila
 * @author edu
 *
 */
public class FinalizaIntervaloLendoDaFila extends AcaoHandler {

	public FinalizaIntervaloLendoDaFila(Configuracao configuracao) {
		super(configuracao);
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {
		
		estado.currentAcao = fila.pop();
		estado.end = estado.currentAcao.getHora();
		incrementaContadoresDeAtividadeComBaseNasAcoes(estado);
		
		if(fazParteDoIntervalo()){
			adicionaAtividade(estado, result);
		}
		
	}


	private boolean fazParteDoIntervalo() {
		return true;
	}

	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado,
			LinkedList<Acao> fila) {
		return new PontoInicial(configuracao);
	}

}
