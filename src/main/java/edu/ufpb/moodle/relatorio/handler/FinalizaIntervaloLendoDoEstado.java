package edu.ufpb.moodle.relatorio.handler;

import java.util.LinkedList;
import java.util.List;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;

public class FinalizaIntervaloLendoDoEstado extends AcaoHandler {

	public FinalizaIntervaloLendoDoEstado(Configuracao configuracao) {
		super(configuracao);
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {

		
		adicionaAtividade(estado, result);

	}

	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado, LinkedList<Acao> fila) {
		return new PontoInicial(configuracao);
	}

}
