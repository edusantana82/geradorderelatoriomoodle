package edu.ufpb.moodle.relatorio.handler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.MutableInterval;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;

public class EstadoDaFila {
	
	protected Acao currentAcao;
	LinkedList<Acao> acoes = new LinkedList<Acao>();
	@Deprecated
	DateTime start;
	@Deprecated
	DateTime end;
	@Deprecated
	public Map<String, Integer> counters= new HashMap<String, Integer>();
	public String ip;

	@Deprecated
	protected Atividade atividade;
	public boolean continua = true;
	
	
	public void clear() {
		atividade = null;
		acoes.clear();
		counters.clear();
		currentAcao = null;
	}
	
	@Deprecated
	public void incrementCountersFromCurrentAcao() {
		incrementCounters(this.currentAcao);
	}
	
	private void incrementCounters(Acao acao) {
		Integer count = counters.get(acao.getAcao());
		Integer newContValue;
		if (count != null) {
			newContValue = count + 1;
		} else {
			newContValue = 1;
		}
		
		counters.put(acao.getAcao(), newContValue);
	}
}
