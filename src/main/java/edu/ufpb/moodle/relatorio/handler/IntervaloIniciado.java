package edu.ufpb.moodle.relatorio.handler;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.Duration;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.ExtratorDeAtividades;

public class IntervaloIniciado extends AcaoHandler {
	static final Logger  logger = Logger.getLogger(IntervaloIniciado.class);
	
	public IntervaloIniciado(Configuracao configuracao) {
		super(configuracao);
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {
	}

	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado,
			LinkedList<Acao> fila) {
		
		AcaoHandler result;
		
		if(fila.isEmpty()){
			result = new Quit(configuracao);
		}else{
			
			Acao peek = fila.peek();
			if(isIpsDiferentes(estado, peek)){
				logger.debug("Descarte: ip");
				result = new DescartaAcao(configuracao);				
			}else if(isNovaAcaoMaiorDoQueLimiteDeIntervalo(estado, peek)){
				logger.debug("Descarte: LimiteDeIntervalo");
				result = new DescartaAcao(configuracao);								
			}else{
				result = new ExtendeIntervaloLendoDaFila(configuracao);				
			}
//			
//			if(isNovaAcaoNoIntervalo(acao, estado)){
//				result = new FinalizaIntervaloLendoDaFila(configuracao);
//			}else{
//				result = new ExtendeIntervaloLendoDaFila(configuracao);
//			}
			
		}
		
		
		

		
		return result;
	}

	private boolean isNovaAcaoMaiorDoQueLimiteDeIntervalo(EstadoDaFila estado,
			Acao peek) {
		
		int limiteDeIntervalo = configuracao.getLimiteDeIntervalo();
		Duration dur = new Duration(estado.acoes.peekLast().getHora(), peek.getHora());
		boolean result = dur.getMillis() > limiteDeIntervalo * 60000;

		
		return result;
	}
	




}
