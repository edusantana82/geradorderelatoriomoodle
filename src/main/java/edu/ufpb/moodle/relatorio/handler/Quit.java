package edu.ufpb.moodle.relatorio.handler;

import java.util.LinkedList;
import java.util.List;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;

public class Quit extends AcaoHandler{

	public Quit(Configuracao configuracao) {
		super(configuracao);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {
		estado.continua = false;
	}

	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado,
			LinkedList<Acao> fila) {
		return null;
	}
	
}
