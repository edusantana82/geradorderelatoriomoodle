package edu.ufpb.moodle.relatorio.handler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;

public class IniciaIntervaloDaFila extends AcaoHandler {

	public IniciaIntervaloDaFila(Configuracao configuracao) {
		super(configuracao);
	}

	@Override
	public void handle(EstadoDaFila estado, LinkedList<Acao> fila,
			List<Atividade> result) {
		estado.currentAcao = fila.pop();
		addAcaoFromCurrentAcao(estado);
		

	}


	@Override
	public AcaoHandler nextHandler(EstadoDaFila estado, LinkedList<Acao> fila) {
		return new IntervaloIniciado(configuracao);
	}


}
