package edu.ufpb.moodle.relatorio;

public class Usuario {
	
	private String nome;
	private String moodleId;
	private String courseId;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMoodleId() {
		return moodleId;
	}

	public void setMoodleId(String moodleId) {
		this.moodleId = moodleId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	@Override
	public String toString() {
		return "Usuario [nome=" + nome + ", moodleId=" + moodleId
				+ ", courseId=" + courseId + "]";
	}

	
}
