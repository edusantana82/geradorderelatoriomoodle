package edu.ufpb.moodle.relatorio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeParser;

public class GeradorDeRelatorioDeUsuario extends Relatorio {

	private final Configuracao configuracao;

	public GeradorDeRelatorioDeUsuario(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	public RelatorioDeUsuario geraRelatorio(List<Acao> acoes) {
		
		if(acoes.isEmpty()){
			throw new IllegalArgumentException("Lista de acões vazia");
		}
		checkConfiguracao();
		
		
		RelatorioDeUsuario relatorioDeUsuario = new RelatorioDeUsuario();
		setUsuario(acoes, relatorioDeUsuario);
		setComponente(acoes, relatorioDeUsuario);
		relatorioDeUsuario.setConfiguracao(configuracao);
		
		setTimesAndSort(acoes);
		
		ExtratorDeAtividades extrator = new ExtratorDeAtividades(configuracao);
		List<Atividade> atividadesAntesDoFiltro = extrator.extrair(acoes);
		List<Atividade> atividades = filtraAtividades(atividadesAntesDoFiltro);
		relatorioDeUsuario.setAtividades(atividades);
		
		return relatorioDeUsuario;
	}

	private void checkConfiguracao() {
		
		if(configuracao.getMeses().isEmpty()){
			throw new IllegalStateException("Os meses não foram configurados.");
		}
		
	}

	private List<Atividade> filtraAtividades(List<Atividade> atividades) {
		return new FiltraAtividades().filtra(atividades, configuracao);
	}

	private void setTimesAndSort(List<Acao> acoes) {
		
		SimpleDateFormat format = new SimpleDateFormat("d MMMMM yyyy, HH:mm");
		for (Acao acaoBean : acoes) {
			Date time;
			try {
				String horario = acaoBean.getHorario();
				horario = horario.trim();
				time = format.parse(horario);
			} catch (ParseException e) {
				throw new RelatorioException(e);
			}
			acaoBean.setHora(new DateTime(time));
		}
		
		Collections.sort(acoes);
		
	}

	private void setComponente(List<Acao> acoes,
			RelatorioDeUsuario relatorioDeUsuario) {
		Acao acaoBean = acoes.get(0);
		relatorioDeUsuario.setComponente(acaoBean.getComponente());

		
	}

	private void setUsuario(List<Acao> acoes,
			RelatorioDeUsuario relatorioDeUsuario) {
		Usuario usuario = new Usuario();
		Acao acaoBean = acoes.get(0);
		usuario.setNome(acaoBean.getNome());
		
		relatorioDeUsuario.setUsuario(usuario);
	}

}
