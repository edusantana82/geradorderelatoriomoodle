package edu.ufpb.moodle.relatorio;

import java.io.File;
import java.io.IOException;

public class Exportador {

	private final Configuracao config;

	public Exportador(Configuracao config) {
		this.config = config;
	}

	public void exporta(Relatorio relatorio) throws IOException {

		
		File file = config.getFileToExport(relatorio);
		file.createNewFile();
		
	}

}
