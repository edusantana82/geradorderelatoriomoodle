package edu.ufpb.moodle.relatorio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

public class Configuracao {

	List<Integer> meses = new LinkedList<Integer>();
	int limiteDeIntervalo = 40;
	private int intervaloMinimo= 10;
	private List<String> exportadores = new LinkedList<String>();
	private List<String> ipsPermitidos = new LinkedList<String>();
	private String sufixo = "";
	private File diretorioDeDestino;
	int duracaoLimiteParaDestaque = 90;
	boolean quiet = false;
	
	
	public static Configuracao readConfiguracaoPadrao() {
		Configuracao config = new Configuracao();
		config.setDiretorioDeDestino(new File("."));
		
		return config;
	}

	public File getFileToExport(Relatorio relatorio) {
		return new File("target/relatorio.html");
	}


	public int getLimiteDeIntervalo() {
		return limiteDeIntervalo;
	}

	public void setLimiteDeIntervalo(int limiteDeIntervalo) {
		this.limiteDeIntervalo = limiteDeIntervalo;
	}

	public void setIntervaloMinimo(int intervaloMinimo) {
		this.intervaloMinimo = intervaloMinimo;
	}

	public int getIntervaloMinimo() {
		return intervaloMinimo;
	}


	public void addExportador(String exportado) {
		this.exportadores.add(exportado);
	}

	public boolean hasExportador(String exportador){
		return this.exportadores.contains(exportador);
	}

	public List<Integer> getMeses() {
		return meses;
	}
	
	public void setMeses(int...meses) {
		for (int mes: meses) {
			this.meses.add(mes);
		}
	}

	public void setMeses(List<Integer> meses) {
		this.meses = meses;
	}

	public String getSufixo() {
		return sufixo;
	}

	public void setSufixo(String sufixo) {
		this.sufixo = sufixo;
	}

	public File getDiretorioDeDestino() {
		return diretorioDeDestino;
	}

	public void setDiretorioDeDestino(File diretorioDeDestino) {
		this.diretorioDeDestino = diretorioDeDestino;
	}

	public static Configuracao readConfiguracaoFromInputStream(
			InputStream in) throws IOException {
		Configuracao configuracao = new Configuracao();
		
		Properties properties = new Properties();
		properties.load(in);

		String mesesFromFile = properties.getProperty(Chave.MESES.name());
		setMeses(configuracao, mesesFromFile);
		
		String ipsPermitidos = properties.getProperty(Chave.IPS_PERMITIDOS.name());
		if(ipsPermitidos!=null){
			for (String ip: ipsPermitidos.split(",")) {
				configuracao.ipsPermitidos.add(ip);
			}			
		}
		
		
		configuracao.setLimiteDeIntervalo(getInt(properties, Chave.LIMITE_DE_INTERVALO));
		configuracao.setIntervaloMinimo(getInt(properties, Chave.INTERVALO_MINIMO));
		configuracao.setDiretorioDeDestino(getFile(properties, Chave.DIRETORIO_DE_DESTINO));
		
		return configuracao;

	}

	/**
	 * 
	 * @param configuracao
	 * @param mesesFromFile meses separados por virgula
	 */
	public static void setMeses(Configuracao configuracao, String mesesFromFile) {
		if(mesesFromFile!= null){
			for (String string : mesesFromFile.split(",")) {
				configuracao.meses.add(Integer.valueOf(string));
			}
			
		}
		
	}

	public void setMeses(String mesesFromFile) {
		for (String string : mesesFromFile.split(",")) {
			meses.add(Integer.valueOf(string));
		}		
	}

	
	
	
	private static void errorIfNoMeses(String mesesFromFile) {
		if("".equals(mesesFromFile) || mesesFromFile == null){
			throw new IllegalStateException("Faltando espefificar os meses nas configuracoes: "+mesesFromFile);
		}
	}

	public static Configuracao readConfiguracaoFromFile(String arquivo) throws FileNotFoundException, IOException {
		InputStream in = new FileInputStream(arquivo);
		return readConfiguracaoFromInputStream(in);
	}
	
	private static File getFile(Properties properties, Chave chave) {
		String file = properties.getProperty(chave.name());
		return file == null ? null : new File(file);
	}

	private static int getInt(Properties properties, Chave chave) {
		String limiteDeIntervalo = properties.getProperty(chave.name());
		return Integer.valueOf(limiteDeIntervalo);
	}

	public static final File ARQUIVO_PADRAO = new File("config.txt");

	enum Chave{
		MESES, LIMITE_DE_INTERVALO, INTERVALO_MINIMO, EXPORTADORES, SUFIXO, DIRETORIO_DE_DESTINO, IPS_PERMITIDOS;
	}
	
	public static void criaArquivoDeConfiguracaoSeNaoExistir() throws FileNotFoundException, IOException {
		if(!ARQUIVO_PADRAO.exists()){
			Configuracao configuracao = readConfiguracaoPadrao();
			Properties props = new Properties();
			
			set(props, Chave.MESES, "");
			set(props, Chave.LIMITE_DE_INTERVALO, configuracao.getLimiteDeIntervalo());
			set(props, Chave.INTERVALO_MINIMO, configuracao.getIntervaloMinimo());
			set(props, Chave.MESES, "");
			set(props, Chave.DIRETORIO_DE_DESTINO, configuracao.getDiretorioDeDestino().getAbsolutePath());
			
			props.store(new FileOutputStream(ARQUIVO_PADRAO), "Configuracao padrao");
			
		}
		
		
	}

	private static void set(Properties props, Chave chave,
			Integer integer) {
		set(props, chave, integer.toString());
	}

	private static void set(Properties props, Chave chave,
			String string) {
		props.put(chave.name(), string);
	}

	public int getDuracaoLimiteParaDestaque() {
		return duracaoLimiteParaDestaque;
	}

	public int getSemanaInicial() {
		MutableDateTime time = new MutableDateTime(new DateTime());
		time.setDayOfMonth(1);
		time.setMonthOfYear(this.meses.get(0));
		return time.getWeekOfWeekyear();
	}

	public int getSemanaFinal() {
		MutableDateTime time = new MutableDateTime(new DateTime());
		time.setDayOfMonth(1);
		time.setMonthOfYear(this.meses.get(meses.size()-1));
		time.addMonths(1);
		time.addDays(-1);
		return time.getWeekOfWeekyear();
	}

	public List<String> getIpsPermitidos() {
		return ipsPermitidos;
	}

	public void setIpsPermitidos(List<String> ipsPermitidos) {
		this.ipsPermitidos = ipsPermitidos;
	}

	@Override
	public String toString() {
		return "Configuracao [meses=" + meses + ", limiteDeIntervalo="
				+ limiteDeIntervalo + ", intervaloMinimo=" + intervaloMinimo
				+ ", exportadores=" + exportadores + ", ipsPermitidos="
				+ ipsPermitidos + ", diretorioDeDestino=" + diretorioDeDestino
				+ ", duracaoLimiteParaDestaque=" + duracaoLimiteParaDestaque
				+ "]";
	}

	public boolean isQuiet() {
		return quiet;
	}

	public void setQuiet(boolean quiet) {
		this.quiet = quiet;
	}

}
