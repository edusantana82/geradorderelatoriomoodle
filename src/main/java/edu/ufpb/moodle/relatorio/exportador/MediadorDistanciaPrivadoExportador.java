package edu.ufpb.moodle.relatorio.exportador;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import au.com.bytecode.opencsv.CSVWriter;

import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class MediadorDistanciaPrivadoExportador {

	public void exporta(RelatorioDeUsuario relatorio) throws IOException {
		File arquivo = getArquivoParaORelatorio(relatorio);

		CSVWriter writer = new CSVWriter(new FileWriter(arquivo), '\t');
		List<String[]> entries = new LinkedList<String[]>();


		DateTimeFormatter dia = DateTimeFormat.forPattern("dd");
		DateTimeFormatter semana = DateTimeFormat.forPattern("E");
		DateTimeFormatter hora = DateTimeFormat.forPattern("HH:mm");
		
		for (Atividade atividade : relatorio.getAtividades()) {
			DateTime start = atividade.getInterval().getStart();
			DateTime end = atividade.getInterval().getEnd();
			Duration duration = new Duration(start, end);
			long horaEmMilis = 1000*60*60L;
			String horas = "" + (duration.getMillis() / horaEmMilis);
			String minutos= "" + ((duration.getMillis() % horaEmMilis) / 60000);
			
			
			String[] entry = new String[]{
					(dia.print(start)),
					(semana.print(start)),
					(hora.print(start)),
					(hora.print(end)),
					horas + ":" + minutos
			};
			
			entries.add(entry);
		}
		
		writer.writeAll(entries);
		writer.close();
	}

	public File getArquivoParaORelatorio(RelatorioDeUsuario relatorio) {
		String userName = relatorio.getUsuario().getNome().replace('*', '_');
		return new File(relatorio.getConfiguracao().getDiretorioDeDestino(), userName + "_" + relatorio.getComponente() + 
				relatorio.getConfiguracao().getSufixo() + ".csv");
	}

}
