package edu.ufpb.moodle.relatorio.exportador;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class ResumoWriter {


	public Resumo createResumo(RelatorioDeUsuario relatorio) {
		Resumo resumo = new Resumo(relatorio);

		DateTime startDate = resumo.getStartDate();
		DateTime endDate = resumo.getEndDate();
		List<Atividade> atividades = relatorio.getAtividades();

		LocalDate date = startDate.toLocalDate();
		while (!date.isAfter(endDate.toLocalDate())) {
			long time = 0;
			for (Atividade atividade : atividades) {
				if (atividade.getInterval().getStart().getDayOfYear() == date
						.getDayOfYear()) {
					time += atividade.getInterval().toDuration().getMillis();
				}

			}
			
			if(time!=0){
				resumo.add(date,time);
			}
			
			date = date.plusDays(1);
		}

		return resumo;
	}


	public void writeResumo(BufferedWriter writer, Resumo resumo) throws IOException {

		DateTime start = resumo.getStartDate();
		DateTime end= resumo.getEndDate();
		DateTime date = start;
		
		StringBuilder sb = new StringBuilder("<p class=\"resumo\">Dias:");
		
		while(date.isBefore(end)){
			
			if(resumo.getTimes(date.toLocalDate()) != 0){
				sb.append(" "+ date.getDayOfMonth()+"/"+date.getMonthOfYear());
			}
			date = date.plusDays(1);
		}
		
		sb.append("</p>");
		
		writer.write(sb.toString());

	}

}
