package edu.ufpb.moodle.relatorio.exportador;

import java.io.File;
import java.io.IOException;

import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class ExportadorDeRelatorioManager {

	private MediadorDistanciaPrivadoExportador exportador = new MediadorDistanciaPrivadoExportador();

	public void exporta(RelatorioDeUsuario relatorio) throws IOException {
		exportador.exporta(relatorio);
	}

	public File getArquivoParaORelatorio(RelatorioDeUsuario relatorio) {
		return exportador.getArquivoParaORelatorio(relatorio);
	}

}
