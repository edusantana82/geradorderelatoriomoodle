package edu.ufpb.moodle.relatorio.exportador;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import edu.ufpb.moodle.relatorio.Atividade;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;

public class MediadorDistanciaHtmlPrivadoExportador {

	private static final String CSS_FILE = "relatorios.css";

	public void exporta(RelatorioDeUsuario relatorio) throws IOException {
		File arquivo = getArquivoParaORelatorio(relatorio);

		BufferedWriter writer = new BufferedWriter(new FileWriter(arquivo));
		writeHtmlHead(writer, relatorio);
		writeTableHeader(writer, relatorio);
		writeEntries(writer, relatorio);
		
		writeEnd(writer, relatorio);

		writeArquivoDeCodigos(relatorio);

		writer.close();
		
		log(relatorio, arquivo);

	}

	private void log(RelatorioDeUsuario relatorio, File arquivo) {
		if(!relatorio.getConfiguracao().isQuiet()){
			System.out.println("Relatorio criado: "+arquivo.getAbsolutePath());
		}
		
	}

	private void writeConfiguracao(BufferedWriter writer,
			RelatorioDeUsuario relatorio) throws IOException {
		int intervaloMinimo = relatorio.getConfiguracao().getIntervaloMinimo();
		int limiteDeIntervalo = relatorio.getConfiguracao().getLimiteDeIntervalo();
		List<Integer> meses = relatorio.getConfiguracao().getMeses();
		
		String p = "\n<p class=\"configuracao\">Configuração executada: intervaloMinimo="+intervaloMinimo+"min limiteDeIntervalo="+limiteDeIntervalo+"min meses="+meses+"</p>";
		
		writer.write(p);

	}

	private void writeArquivoDeCodigos(RelatorioDeUsuario relatorio)
			throws IOException {
		writeUsuariosCodigos(relatorio);
		writeComponentesCodigos(relatorio);

	}

	private void writeComponentesCodigos(RelatorioDeUsuario relatorio)
			throws IOException {
		Properties properties = new Properties();
		File file = new File(relatorio.getConfiguracao()
				.getDiretorioDeDestino(), "componentes.txt");
		if (file.exists()) {
			properties.load(new FileInputStream(file));
		}
		properties
				.put(getComponentDigest(relatorio), relatorio.getComponente());
		properties.store(new FileOutputStream(file), "");

	}

	private void writeUsuariosCodigos(RelatorioDeUsuario relatorio)
			throws IOException {

		Properties properties = new Properties();

		File file = new File(relatorio.getConfiguracao()
				.getDiretorioDeDestino(), "usuarios.txt");
		if (file.exists()) {
			properties.load(new FileInputStream(file));
		}
		properties.put(getUserDigest(relatorio), relatorio.getUsuario()
				.getNome());

		properties.save(new FileOutputStream(file), "");

	}

	private void writeEnd(BufferedWriter writer, RelatorioDeUsuario relatorio)
			throws IOException {
		writer.write("\n</table>");
		
		writeResumo(writer, relatorio);
		writeConfiguracao(writer, relatorio);
		//writeHoratio(writer, relatorio);
		writeFootNote(writer, relatorio);
		String end = "</body></html>";
		writer.write(end);

	}

	private ResumoWriter resumoWriter = new ResumoWriter(); 
	private void writeResumo(BufferedWriter writer, RelatorioDeUsuario relatorio) throws IOException {
		Resumo resumo = resumoWriter.createResumo(relatorio);
		resumoWriter.writeResumo(writer, resumo);
	}

	private void writeFootNote(BufferedWriter writer,
			RelatorioDeUsuario relatorio) throws IOException {
		String foot= "\n<!--Developer: Eduardo Santana (eduardo.ufpb@gmail.com).-->\n";
		writer.write(foot);		
	}

	private void writeHoratio(BufferedWriter writer,
			RelatorioDeUsuario relatorio) throws IOException {
		String horario = "\n<h2>Horário</h2>\n<table>" +
				"<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"+
				"<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"+
				"</table>";
		writer.write(horario);
	}

	private void writeEntries(BufferedWriter writer,
			RelatorioDeUsuario relatorio) throws IOException {

		DateTimeFormatter dia = DateTimeFormat.forPattern("dd");
		DateTimeFormatter semana = DateTimeFormat.forPattern("E");
		DateTimeFormatter hora = DateTimeFormat.forPattern("HH:mm");
		StringBuilder builder = new StringBuilder();

		for (Atividade atividade : relatorio.getAtividades()) {
			DateTime start = atividade.getInterval().getStart();
			DateTime end = atividade.getInterval().getEnd();
			Duration duration = new Duration(start, end);
			
			String mes = "" + start.getMonthOfYear();
			String cssClass=getEntryCssClass(atividade);
			
			builder.append("\n<tr class=\""+cssClass+"\">");
			addColumn(builder, mes + "-"+ start.getWeekOfWeekyear());
			addSemana(semana, builder, start);
			addColumn(builder, dia.print(start));
			addIP(builder, atividade);
			addColumn(builder, hora.print(start));
			addColumn(builder, hora.print(end));
			addDuracao(builder, duration, relatorio.getConfiguracao());
			builder.append("<td></td>"); // observacões

			builder.append("</tr>");
			
			writeAcoesDetalhes(builder, relatorio, atividade);
		}

		
		
		writer.write(builder.toString());

	}

	private void addSemana(DateTimeFormatter semana, StringBuilder builder,
			DateTime start) {
		addColumn(builder, semana.print(start));
	}

	private String getEntryCssClass(Atividade atividade) {
		int weekDay = atividade.getInterval().getStart().getDayOfWeek();
		return (weekDay == 6) || (weekDay == 7) ? "finalDeSamana" : "semana";
	}

	private void addDuracao(StringBuilder builder, Duration duration, Configuracao configuracao) {
		long horaEmMilis = 1000 * 60 * 60L;
		int duracaoEmMinutos = (int) (duration.getMillis() / 60000);
		String spanClass;
		if(duracaoEmMinutos < configuracao.getDuracaoLimiteParaDestaque()){
			spanClass = "menorQueLimite";
		}else{
			spanClass = "maiorQueLimite";
		}
		
		
		String horas = "" + (duration.getMillis() / horaEmMilis);
		String minutos = ""
				+ ((duration.getMillis() % horaEmMilis) / 60000);
		String time = horas + ":" + minutos;
		addColumn(builder, "<span class=\""+spanClass+"\">"+time+"</span>");
	}

	private void addIP(StringBuilder builder, Atividade atividade) {
		String ipValue;
		if(atividade.getIp().startsWith("192.168.11")){
			ipValue = "laboratorio";
		}else if(atividade.getIp().startsWith("150.165")){
			ipValue = "universidade";
		}else if(atividade.getIp().startsWith("192.168.0")){
			ipValue = "local ou lab wifi";
		}else if(atividade.getIp().startsWith("187") || atividade.getIp().startsWith("189")){
			ipValue = "<span class=\"operadora\">oi/velox</span>";
		}else if(atividade.getIp().startsWith("200")){
			ipValue = "<span class=\"operadora\">gvt</span>";
		}else{
			ipValue = "<span class=\"operadora\">"+atividade.getIp()+"</span>";
		}
		addColumn(builder, ipValue);
	}

	private void writeAcoesDetalhes(StringBuilder builder,
			RelatorioDeUsuario relatorio, Atividade atividade) throws IOException {
		builder.append("<tr class=\"acoes\"><td colspan=\"8\">");
		
		Set<Entry<String, Integer>> entrySet = atividade.getAcaoCount().entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			builder.append(" " + traduzAcao(entry) + "="+ entry.getValue());
		}
		
		
		builder.append("</td></tr>");
	}

	Properties dicionario;
	
	private String traduzAcao(Entry<String, Integer> entry) throws IOException {
		if(dicionario == null){
			dicionario = new Properties();
			InputStream inputStream = ClassLoader.getSystemResourceAsStream("traducao.properties");
			dicionario.load(inputStream);
		}
		
		String result;
		String traducao = dicionario.getProperty(entry.getKey()); 
		if( traducao == null){
			result = entry.getKey();
		}else{
			result = traducao;
		}
		
		return result;
		
	}


	private void addColumn(StringBuilder builder, String string) {
		builder.append("<td>");
		builder.append(string);
		builder.append("</td>");
	}

	private void writeTableHeader(BufferedWriter writer,
			RelatorioDeUsuario relatorio) throws IOException {

		String userCode = relatorio.getUsuario().getNome();
		String componentCode = relatorio.getComponente();

		String comment = createComment(writer, relatorio);
		String userLink = getUserLink(relatorio);

		String usuario = "<h1>Usuario: <a href=\"usuarios.txt\">" + userCode
				+ "</a></h1>";
		String componente = "<h2>Componente: <a href=\"componentes.txt\">"
				+ componentCode + "</a></h2>";

		writer.write(comment);
		writer.write(usuario);
		writer.write(componente);
		writer.write("\n<table><tr><th width=\"10\">Mes</th><th width=\"15\">Dia</th><th width=\"10\">Data</th><th width=\"30\">IP</th><th width=\"20\">Entrada</th><th width=\"20\">Saida</th><th width=\"20\">Tempo</th><th>Observações</th></tr>");
		

	}

	private String getUserLink(RelatorioDeUsuario relatorio) {
		//TODO fazer um link para o moodle?
		return null;
	}

	private String createComment(BufferedWriter writer,
			RelatorioDeUsuario relatorio) {
		String nome = relatorio.getUsuario().getNome();
		String comment = "\n<!-- " + nome + "-->\n";
		comment += "<!-- " + relatorio.getConfiguracao().toString() + "-->\n";

		return comment;
	}

	protected void writeHtmlHead(BufferedWriter writer, RelatorioDeUsuario relatorio)
			throws IOException {
		String cssStyle = getCssStyle();
		String head = "<html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"+cssStyle+"</head><body>";

		writer.write(head);

	}

	private String getCssStyle() throws IOException {
		String inicio = "\n<style type=\"text/css\">\n";
		String fim = "\n</style>\n";
		String meio = "";


		
		StringBuilder sb = new StringBuilder();
		ClassLoader cl = getClass().getClassLoader();
		InputStream resource = cl.getResourceAsStream(CSS_FILE);
		
		int c;
		
		while ((c = resource.read())!=-1){
			sb.append((char)c);
		}
		
		meio = sb.toString();
		
		return inicio+meio+fim;
	}

	public File getArquivoParaORelatorio(RelatorioDeUsuario relatorio) {

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");

			md.update(relatorio.getUsuario().getNome().getBytes());
			md.update(relatorio.getComponente().getBytes());

			byte[] digest = md.digest();

			return new File(
					relatorio.getConfiguracao().getDiretorioDeDestino(),
					relatorio.getComponente()+"_"+
					byteArray2Hex(digest) + ".html");

		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}

	}

	String getComponentDigest(RelatorioDeUsuario relatorio) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(relatorio.getComponente().getBytes());
			return byteArray2Hex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
	}

	String getUserDigest(RelatorioDeUsuario relatorio) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");

			md.update(relatorio.getUsuario().getNome().getBytes());
			return byteArray2Hex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
	}

	private static String byteArray2Hex(byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		return formatter.toString();
	}

}
