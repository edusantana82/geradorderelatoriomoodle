package edu.ufpb.moodle.relatorio.exportador;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import edu.ufpb.moodle.relatorio.Configuracao;

public class CSSCreator {

	public void create(Configuracao configuracao) throws IOException {
		InputStream in = ClassLoader.getSystemResourceAsStream(getCssFileName());
		File file = new File(configuracao.getDiretorioDeDestino(), getCssFileName());
		FileOutputStream outputStream = new FileOutputStream(file);
		
		
		int c;
		while((c=in.read())!= -1){
			outputStream.write(c);
		}
		outputStream.close();
		
	}

	private String getCssFileName() {
		return "relatorios.css";
	}

}
