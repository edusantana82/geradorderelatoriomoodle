package edu.ufpb.moodle.console;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.cli.CommandLine;

import edu.ufpb.moodle.relatorio.Acao;
import edu.ufpb.moodle.relatorio.Configuracao;
import edu.ufpb.moodle.relatorio.GeradorDeRelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.MoodleFileReader;
import edu.ufpb.moodle.relatorio.RelatorioDeUsuario;
import edu.ufpb.moodle.relatorio.RelatorioMain;
import edu.ufpb.moodle.relatorio.exportador.CSSCreator;
import edu.ufpb.moodle.relatorio.exportador.MediadorDistanciaHtmlPrivadoExportador;

public class HtmlReportOperacao implements Operacao {

	private final Console console;

	public HtmlReportOperacao(Console console) {
		this.console = console;
	}

	private static final FilenameFilter TXT_FILTER = new FilenameFilter() {
		
		public boolean accept(File arg0, String name) {
			return name.endsWith("txt") && ! ("componentes.txt".equals(name) || "usuarios.txt".equals(name));
		}
	};

	
	public void execute(CommandLine line) throws IOException {
		if(checkPreCondition(line)){
			File[] files = getInputFiles(line);


			Configuracao configuracao = loadConfiguracao(line);

			
			for (final File file : files) {
				try {
					MoodleFileReader moodleFileReader = new MoodleFileReader();
					List<Acao> acoes = moodleFileReader.lerArquivo(file);
					GeradorDeRelatorioDeUsuario gerador = new GeradorDeRelatorioDeUsuario(
							configuracao);
					RelatorioDeUsuario relatorio = gerador.geraRelatorio(acoes);
					MediadorDistanciaHtmlPrivadoExportador exportador = new MediadorDistanciaHtmlPrivadoExportador(){
						@Override
						public File getArquivoParaORelatorio(
								RelatorioDeUsuario relatorio) {
							File target = super.getArquivoParaORelatorio(relatorio);
							File newTarget = new File(target.getParentFile(), file.getName() + ".html");
							return newTarget;
						}
					};
					exportador.exporta(relatorio);
				} catch (Exception e) {
					System.err.println("Erro no arquivo: " + file.getAbsolutePath());
					System.err.println(e.getLocalizedMessage());
					//
					e.printStackTrace();
				}
				
			}
		}
		
		
	}


	private boolean checkPreCondition(CommandLine line) {
		boolean pass = true;
		
		
		if(!line.hasOption(console.input.getOpt())){
			System.err.println("parametro input faltando");
			pass = false;
		}
		
		
		return pass;
	}


	private File[] getInputFiles(CommandLine line) {
		File input = getInputFile(line);
		File[] files;
		if(input.isDirectory()){
			files = input.listFiles(TXT_FILTER);
		}else{
			files=new File[]{input};
		}
		return files;
	}


	private File getInputFile(CommandLine line) {
		return new File(line.getOptionValue(console.input.getOpt()));
	}


	private Configuracao loadConfiguracao(CommandLine line)
			throws FileNotFoundException, IOException {
		Configuracao configuracao;
		
		if(line.hasOption(console.config.getOpt())){
			configuracao = Configuracao.readConfiguracaoFromFile(line
					.getOptionValue(console.config.getOpt()));
		}else{
			InputStream inputStream = ClassLoader.getSystemResourceAsStream("config.properties");
			configuracao = Configuracao.readConfiguracaoFromInputStream(inputStream);
		}
		
		setIntervaloMinimo(configuracao, line);
		setMeses(configuracao, line);
		setDiretorioDestino(configuracao, line);
		setQuiet(configuracao, line);
		checkConfiguracao(configuracao);
		
		return configuracao;
	}

	


	private void setQuiet(Configuracao configuracao, CommandLine line) {
		configuracao.setQuiet(line.hasOption(console.quiet.getOpt()));
		
	}


	private void checkConfiguracao(Configuracao configuracao) {
		
		if(configuracao.getMeses().isEmpty()){
			throw new IllegalStateException("Os meses não foram configurados.");
		}
		
	}

	

	private void setDiretorioDestino(Configuracao configuracao, CommandLine line) {
		if(line.hasOption(console.output.getOpt())){
			File diretorioDeDestino = new File(line.getOptionValue(console.output.getOpt()));
			configuracao.setDiretorioDeDestino(diretorioDeDestino);
		}else{
			File file = getInputFile(line);
			if(file.isDirectory()){
				configuracao.setDiretorioDeDestino(file);
			}else{
				configuracao.setDiretorioDeDestino(file.getParentFile());
			}
		}
	}


	private void setMeses(Configuracao configuracao, CommandLine line) {
		if(line.hasOption(console.mes.getOpt())){
			configuracao.setMeses(line.getOptionValue(console.mes.getOpt()));
		}else{
			configuracao.setMeses(1,2,3,4,5,6,7,8,9,10,11,12);
		}
		
	}
	
	private void setIntervaloMinimo(Configuracao configuracao, CommandLine line) {
		if(line.hasOption(console.intervaloMinimo.getOpt())){
			int intervaloMinimo = Integer.valueOf(line.getOptionValue(console.intervaloMinimo.getOpt()));
			configuracao.setIntervaloMinimo(intervaloMinimo);
		}
	}

}
