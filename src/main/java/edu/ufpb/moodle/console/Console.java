package edu.ufpb.moodle.console;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

@SuppressWarnings("static-access")
public class Console {

	Options options = new Options();
	Option input = OptionBuilder
			.withArgName("fonte")
			.withLongOpt("input")
			.hasArg()
			.withDescription(
					"log de acesso do moodle, ou diretório de entrada contendo os logs, ou arquivo txt com links dos perfis para gerar o iMacro")
			.create('i');
	Option output = OptionBuilder.withArgName("destino").hasArg()
			.withLongOpt("output").withDescription("diretório de destino")
			.create('o');
	Option config = OptionBuilder
			.withArgName("file")
			.isRequired(false)
			.hasArg()
			.withDescription("arquivo de configuracao de execucao do relatorio. Você provavelmente não irá precisar.")
			.create("config");

	Option mes = OptionBuilder
			.withArgName("meses")
			.hasArg()
			.withDescription(
					"meses do relatorio, separados por virgula e entre aspas. Se ausente, todos serão considerados.")
			.withLongOpt("meses")
			.create('m');
	Option intervaloMinimo = OptionBuilder
			.withArgName("intervaloMinimo")
			.hasArg()
			.withDescription(
					"intervalo minimo do relatorio de acesso")
					.withLongOpt("intervaloMinimo")
					.create('n');
	Option macro = OptionBuilder.withLongOpt("gera-macro")
			.withDescription("gera arquivo no formato iMacro para baixar os logs de acesso do Moodle")
			.create('M');
	Option relatorio = OptionBuilder.withLongOpt("gera-relatorio")
			.withDescription("gera relatório html. Esta é a principal função deste programa.").create('r');
	Option help = new Option("help", "imprime esta ajuda de uso.");
	Option quiet = OptionBuilder.withLongOpt("quiet")
			.withDescription("executa sem imprimir informacoes.").create('q');
	
	{
		OptionGroup groupOperation = new OptionGroup();
		groupOperation.addOption(relatorio);
		groupOperation.addOption(macro);
		groupOperation.addOption(help);
		groupOperation.setRequired(true);
		options.addOptionGroup(groupOperation);
		options.addOption(input);
		options.addOption(output);
		options.addOption(config);
		options.addOption(mes);
		options.addOption(intervaloMinimo);
		options.addOption(quiet);
		
		
//		options.addOption(macro);
//		options.addOption(relatorio);
//		options.addOption(help);
	}

	public static void main(String[] args) throws IOException {
		Console console = new Console();
		console.execute(args);
	}

	private void execute(String[] args) throws IOException {
		
		CommandLineParser parser = new GnuParser();
		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args);
			interrogation(line);
		} catch (ParseException exp) {
			System.err.println("Parsing failed.  Reason: " + exp.getMessage());
			Operacao operacao = new HelpOperacao(options);
			operacao.execute(null);

		}
		
	}


	private void interrogation(CommandLine line) throws IOException {
		if (line.hasOption(help.getOpt())) {
			Operacao operacao = new HelpOperacao(options);
			operacao.execute(line);
		} else if (line.hasOption(macro.getOpt())) {
			Operacao operacao = new MacroCriationOperacao(this);
			operacao.execute(line);
		} else if (line.hasOption(relatorio.getOpt())) {
			// gera relatorio html
			Operacao operacao = new HtmlReportOperacao(this);
			operacao.execute(line);
		} else {
			Operacao operacao = new HelpOperacao(options);
			operacao.execute(line);
		}
	}

}
