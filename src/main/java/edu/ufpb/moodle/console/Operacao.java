package edu.ufpb.moodle.console;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;

public interface Operacao {

	public void execute(CommandLine line) throws IOException;
	
}
