package edu.ufpb.moodle.console;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

public class HelpOperacao implements Operacao {

	private final Options options;

	public HelpOperacao(Options options) {
		this.options = options;
	}

	public void execute(CommandLine line) throws IOException {
		
		InputStream inputStream = ClassLoader.getSystemResourceAsStream("ajuda.txt");
		Reader in = new InputStreamReader(inputStream);
		
		int c;
		
		while ((c=in.read())!=-1){
			System.out.print((char)c);
		}
		
		
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "java -jar gerador.jar", options, true );
	}

}
