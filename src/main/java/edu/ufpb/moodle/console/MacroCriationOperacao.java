package edu.ufpb.moodle.console;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.log4j.Logger;

import edu.ufpb.moodle.imacro.LeitorDePlanilha;
import edu.ufpb.moodle.imacro.LeitorDePlanilhaHyperlinks;
import edu.ufpb.moodle.imacro.PlanilhaDocentes;
import edu.ufpb.moodle.relatorio.ExtratorDeAtividades;
import edu.ufpb.moodle.relatorio.imacro.IMacroScriptGenerator;

public class MacroCriationOperacao implements Operacao {

	public static final String NOME_PADRAO_DO_MACRO = "baixar-logs-do-moodle.iim";

	static final Logger  logger = Logger.getLogger(MacroCriationOperacao.class);
	
	private final Console console;

	public MacroCriationOperacao(Console console) {
		this.console = console;
	}

	public void execute(CommandLine line) throws IOException {
		File PLANILHA_DOCENTES = new File(line.getOptionValue(console.input.getOpt()));
		LeitorDePlanilha leitor = new LeitorDePlanilhaHyperlinks(PLANILHA_DOCENTES);
		
		PlanilhaDocentes docentes = leitor.lerPlanilha();

		IMacroScriptGenerator generator = new IMacroScriptGenerator();
		
		String script = generator.generate(docentes);
		
		File imacroFile;
		
		if(line.hasOption(console.output.getOpt())){
			String file = line.getOptionValue(console.output.getOpt());
			imacroFile = new File(file);
		}else{
			imacroFile = new File(PLANILHA_DOCENTES.getParentFile(), NOME_PADRAO_DO_MACRO);
		}
		
		generator.createFile(script, imacroFile);

		if(!line.hasOption(console.quiet.getOpt())){
			System.out.println("Arquivo criado: " + imacroFile.getAbsolutePath());
		}
	}

}
